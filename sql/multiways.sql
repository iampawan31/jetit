-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 26, 2015 at 04:35 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `jetit`
--

-- --------------------------------------------------------

--
-- Table structure for table `multiways`
--

CREATE TABLE IF NOT EXISTS `multiways` (
`mway_id` int(11) NOT NULL,
  `flightfrom` varchar(255) NOT NULL,
  `flightto` varchar(255) NOT NULL,
  `departuredate` date NOT NULL,
  `departuretime` time NOT NULL,
  `noofpassangers` int(10) NOT NULL,
  `jettype` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `contact` int(10) NOT NULL,
  `email` varchar(100) NOT NULL,
  `comments` varchar(255) NOT NULL,
  `dateofregister` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `multiways`
--

INSERT INTO `multiways` (`mway_id`, `flightfrom`, `flightto`, `departuredate`, `departuretime`, `noofpassangers`, `jettype`, `firstname`, `lastname`, `contact`, `email`, `comments`, `dateofregister`) VALUES
(1, 'pune', 'delhi', '2015-08-28', '01:30:00', 12, 'Super Mid-Size', 'hemant', 'arya', 2147483647, 'arya.hemant322@gmail.com', 'dsdsds', '2015-08-26'),
(2, 'pune', 'delhi', '2015-08-28', '01:00:00', 12, 'Mid-Size', 'hemant', 'arya', 78, 'arya.hemant322@gmail.com', 'fdsfdsfs', '2015-08-26'),
(3, 'pune', 'delhi', '2015-08-29', '12:30:00', 5, 'Light-Jet', 'hemant', 'arya', 78, 'arya.hemant322@gmail.com', 'dfdfdf', '2015-08-26'),
(4, 'pune', 'delhi', '2015-08-27', '12:30:00', 12, 'Turbo Prop', 'hemant', 'arya', 78, 'arya.hemant322@gmail.com', 'fdsfdsfdsf', '2015-08-26');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `multiways`
--
ALTER TABLE `multiways`
 ADD PRIMARY KEY (`mway_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `multiways`
--
ALTER TABLE `multiways`
MODIFY `mway_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
