-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 26, 2015 at 04:35 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `jetit`
--

-- --------------------------------------------------------

--
-- Table structure for table `legs`
--

CREATE TABLE IF NOT EXISTS `legs` (
`leg_id` int(11) NOT NULL,
  `mway_id` int(10) NOT NULL,
  `flightfrom` varchar(255) NOT NULL,
  `flightto` varchar(255) NOT NULL,
  `departuredate` date NOT NULL,
  `departuretime` time NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `legs`
--

INSERT INTO `legs` (`leg_id`, `mway_id`, `flightfrom`, `flightto`, `departuredate`, `departuretime`) VALUES
(1, 0, 'add1', 'add2', '1970-01-01', '00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `legs`
--
ALTER TABLE `legs`
 ADD PRIMARY KEY (`leg_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `legs`
--
ALTER TABLE `legs`
MODIFY `leg_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
