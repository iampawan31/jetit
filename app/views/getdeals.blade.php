@extends('layouts.master')
@section('content')
<section class="sub-banner" style="margin-top: 82px;">
            <!--Background-->
            <div class="bg-parallax bg-1" style="background-position: 50% 8px;"></div>
            <!--End Background-->
            <!-- Logo -->
            <div class="logo-banner text-center" style="display: none;">
                <a href="" title="">
                    <img src="images/logo-banner.png" alt="">
                </a>
            </div>
            <!-- Logo -->
        </section>
<div class="main">
            <div class="container">
                <div class="main-cn hotel-page bg-white">
                    <div class="row">
                        
                        <!-- Hotel Right -->
                        <div class="col-md-12">

                     
                            <!-- End Breakcrumb -->

                            <!-- Hotel List -->
                            <section  class="about-cn clearfix">
            <div class="element-cn" >
                            <div class="tab-content">
                                <div id="section4" class="tab-pane fade active in">
                                    <div class="sales-cn" style="margin-top: 2%;">
                                    <div class="row warlock-warning">
                                        @if ($msg)
                                            {{$msg}}
                                        @endif
                                    </div>
                    <div class="row">
                        <!-- HostSales Item -->
                        @foreach ($result as $res)
                           
                        
                        <div class="col-xs-6 col-md-4">
                            <div class="sales-item">
                                <figure class="home-sales-img">
                                    <a href="#" title="">
                                      <img src="{{asset($res->cityimage)}}" onmouseover="this.src='{{asset($res->flightimage)}}'" onmouseout="this.src='{{asset($res->cityimage)}}'" style="height: 293px;" border="0" alt="">
                                    </a>
                                    <figcaption>
                                        Save <span>{{$res->discount}}</span>%
                                    </figcaption>
                                </figure>
                                <div class="home-sales-text">
                                    <div class="home-sales-name-places">
                                        <div class="home-sales-name">
                                            <a href="#" title="">{{$res->to}}</a>
                                        </div>
                                    </div>
                                    @if ($res->flighttype == 1)
                                        International
                                    @else
                                        Domestic
                                    @endif
                                    <div class="price-box">
                                        <span class="price old-price">From : {{$res->from}}</span>
                                        <span class="price special-price">{{$res->price}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <div class="row">
                            {{HTML::linkRoute('jetdeals','ALL DEALS',array(), array('class' => 'awe-btn awe-btn-5 awe-btn-lager arrow-right text-uppercase float-right'))}}
                    </div>
                </div>
                                </div>
                               
                            </div>

                        </div>
                        </section>
                            <!-- End Hotel List -->

                        </div>
                        <!-- End Hotel Right -->

                        <!-- Sidebar Hotel -->
                        
                        <!-- End Sidebar Hotel -->

                        

                    </div>

                </div>
            </div>
        </div>
@stop