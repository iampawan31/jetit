<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="_token" content="{!! csrf_token() !!}"/>
    <title>{{$title}}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="format-detection" content="telephone=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    {{HTML::style('http://fonts.googleapis.com/css?family=Lato:300,400%7COpen+Sans:300,400,600')}}
    {{HTML::style('admin/css/bootstrap.min.css')}}
    {{HTML::style('admin/css/plugins/jquery-ui/jquery-ui.min.css')}}
    {{HTML::style('admin/css/plugins/pageguide/pageguide.css')}}
    {{HTML::style('admin/css/plugins/fullcalendar/fullcalendar.css')}}
    {{HTML::style('admin/css/plugins/fullcalendar/fullcalendar.print.css')}}
    {{HTML::style('admin/css/plugins/chosen/chosen.css')}}
    {{HTML::style('admin/css/plugins/select2/select2.css')}}
    {{HTML::style('admin/css/plugins/icheck/all.css')}}
    {{HTML::style('admin/css/style.css')}}
    {{HTML::style('css/warlock.css')}}
    {{HTML::style('admin/css/themes.css')}}
     {{HTML::style('css/library/jquery.timepicker.css')}}
    {{ HTML::script('admin/js/jquery.min.js') }}
    {{ HTML::script('admin/js/plugins/nicescroll/jquery.nicescroll.min.js') }}
    {{ HTML::script('admin/js/plugins/jquery-ui/jquery-ui.js') }}
    {{ HTML::script('admin/js/plugins/touch-punch/jquery.touch-punch.min.js') }}
    {{ HTML::script('admin/js/plugins/slimscroll/jquery.slimscroll.min.js') }}
    {{ HTML::script('admin/js/bootstrap.min.js') }}
    {{ HTML::script('admin/js/plugins/vmap/jquery.vmap.min.js') }}
    {{ HTML::script('admin/js/plugins/vmap/jquery.vmap.world.js') }}
    {{ HTML::script('admin/js/plugins/vmap/jquery.vmap.sampledata.js') }}
    {{ HTML::script('admin/js/plugins/bootbox/jquery.bootbox.js') }}
    {{ HTML::script('admin/js/plugins/flot/jquery.flot.min.js') }}
    {{ HTML::script('admin/js/plugins/flot/jquery.flot.bar.order.min.js') }}
    {{ HTML::script('admin/js/plugins/flot/jquery.flot.pie.min.js') }}
    {{ HTML::script('admin/js/plugins/flot/jquery.flot.resize.min.js') }}
    {{ HTML::script('admin/js/plugins/imagesLoaded/jquery.imagesloaded.min.js') }}
    {{ HTML::script('admin/js/plugins/pageguide/jquery.pageguide.js') }}
    {{ HTML::script('admin/js/plugins/fullcalendar/moment.min.js') }}
    {{ HTML::script('admin/js/plugins/fullcalendar/fullcalendar.min.js') }}
    {{ HTML::script('admin/js/plugins/chosen/chosen.jquery.min.js') }}
    {{ HTML::script('admin/js/plugins/select2/select2.min.js') }}
    {{ HTML::script('admin/js/plugins/icheck/jquery.icheck.min.js') }}
    {{ HTML::script('admin/js/eakroko.min.js') }}
    {{ HTML::script('admin/js/application.min.js') }}
    {{ HTML::script('admin/js/demonstration.min.js') }}
     {{ HTML::script('js/library/jquery.timepicker.min.js') }}
    

<body class="theme-orange" data-theme="theme-orange">
<div id="navigation">
        <div class="container-fluid">
            <a href="#" id="brand">Jetit</a>
           
            <ul class='main-nav'>
                <li class='active'>
                    {{HTML::linkRoute('admin','Post A Deal')}}
                </li>
                <li class='active'>
                    {{HTML::linkRoute('history','Deal Log')}}
                </li>
                <li class='active'>
                    {{HTML::linkRoute('charter','Oneway Log')}}
                </li>
                 <li class='active'>
                    {{HTML::linkRoute('multileg','Multileg Log')}}
                </li>
               
            </ul>
            <div class="user">
                <div class="dropdown">
                    <a href="#" class='dropdown-toggle' data-toggle="dropdown">{{Auth::user()->username}}
                        
                    </a>
                    <ul class="dropdown-menu pull-right">
                        <li>
                            <a href="more-userprofile.html">Edit profile</a>
                        </li>
                        <li>
                            <a href="#">Account settings</a>
                        </li>
                        <li>
                            <a href="logout">Sign out</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    
       

    @yield('content')
    

    

</div>


</body>
</html>
