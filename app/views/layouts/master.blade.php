<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="_token" content="{!! csrf_token() !!}"/>
    <title>{{$title}}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="format-detection" content="telephone=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    {{HTML::style('http://fonts.googleapis.com/css?family=Lato:300,400%7COpen+Sans:300,400,600')}}
    {{HTML::style('css/style.css')}}
    {{HTML::style('css/stylesheet.css')}}
    {{HTML::style('css/slick_fitp.css')}}
    {{HTML::style('css/warlock.css')}}
    {{HTML::style('css/library/bootstrap.min.css')}}
    {{HTML::style('css/library/font-awesome.min.css')}}
    {{HTML::style('css/library/jquery-ui.min.css')}}
    {{HTML::style('css/library/owl.carousel.css')}}
    {{HTML::style('css/library/jquery-ui.css')}}
    {{HTML::style('css/library/jquery.timepicker.css')}}
    {{ HTML::script('js/jquery-1.11.0.min.js') }}
    {{ HTML::script('js/jquery-ui.min.js') }}
    {{ HTML::script('js/library/bootstrap.min.js') }}
    {{ HTML::script('js/jquery.leanModal.min.js') }}
    {{ HTML::script('js/library/SmoothScroll.js') }}
    {{ HTML::script('http://maps.google.com/maps/api/js?sensor=false') }}
    {{ HTML::script('js/library/owl.carousel.min.js') }}
    {{ HTML::script('js/library/parallax.min.js') }}
    {{ HTML::script('js/library/jquery.nicescroll.js') }}
    {{ HTML::script('js/library/jquery.ui.touch-punch.min.js') }}
    {{ HTML::script('js/library/jquery.timepicker.min.js') }}
    <script type="text/javascript">
$.ajaxSetup({
   headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
});
</script>
    <script type="text/javascript">
      $(window).load(function() {
            // Preloader
            $("#switch").click(function() {
                $(this).toggleClass("on");
                $("#links").toggleClass("odprt zaprt");
            });
        });
  </script> 
  <!-- for One way -->
  <script>
      $(document).ready(function() {
    var max_fields      = 2; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var wrapper1         = $(".input_fields_wrap1"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID

    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div><input type="text" placeholder="Return Date" class="form-control datepicker1" name="mytext[]" style="margin-bottom: 5%;margin-top: 5%;"/></div>'); //add input box
            $(wrapper1).append('<div><input type="text" placeholder="Return Time" name="mytext2[]" class="form-control time" style="margin-bottom: 5%;margin-top: 5%;"/></div>'); //add input box
            
        }
    });

    
});
</script>
<!-- for multi destination -->
<script type="text/javascript">
    
(function() {  
    var dialog = document.getElementById('window');  
    document.getElementById('show').onclick = function() {  
        dialog.show();  
    };  
    document.getElementById('exit').onclick = function() {  
        dialog.close();  
    };  
})();  

</script>
<script>
    $(document).ready(function(){
        $(document).on('click','.signup-tab',function(e){
            e.preventDefault();
            $('#signup-taba').tab('show');
        }); 

        $(document).on('click','.signin-tab',function(e){
            e.preventDefault();
            $('#signin-taba').tab('show');
        });

        $(document).on('click','.forgetpass-tab',function(e){
            e.preventDefault();
            $('#forgetpass-taba').tab('show');
        });
    }); 
</script>

<style>
    .bt-login,.bt-login:hover, .bt-login:active, .bt-login:focus {
        background-color: #E9AD41;
        color: #ffffff;
        padding-bottom: 10px;
        padding-top: 10px;
        transition: background-color 300ms linear 0s;
        float: right;
    }


    .login-tab {
        margin: 0 auto;
        max-width: 100%;
    }

    .login-modal-header {
        background: #E9AD41;
        color: #fff;
    }

    .login-modal-header .modal-title {
        color: #fff;
    }

    .login-modal-header .close {
        color: #fff;
    }

    .login-modal i {
        color: #000;
    }

    .login-modal form {
        max-width: 100%;
    }

    .tab-pane form {
        margin: 0 auto;
    }
    .login-modal-footer{
        margin-top:15px;
        margin-bottom:15px;
    }
    dialog {  
    width: 500px;  
    background:#e8e8e8;
    border: 1px solid #dadada;
    font-family:sans-serif;
    padding: 5px 10px 20px 20px;
} 
</style>
</head>
<body>
    
    <div id="wrap">

        <!-- Header -->
        <header id="header" class="header" style="background-color: rgb(255, 255, 255);">
            <div class="container">
                <!-- Logo -->
                <div class="logo float-left">
                    <a href="/" title=""><img src="{{URL::asset('images/logo.png')}}" alt=""></a>
                </div>
                <!-- End Logo -->
                <!-- Bars -->
                <div class="bars" id="bars"></div>
                <!-- End Bars -->

                <!--Navigation-->
                <nav class="navigation nav-c" id="navigation" data-menu-type="1200">
                    <div class="nav-inner">
                        <a href="#" class="bars-close" id="bars-close" style="display: none;">Close</a>
                        <div class="tb">
                            <div class="tb-cell">
                                <ul class="menu-list text-uppercase">
                                 @if(Request::url() === 'http://jetit.dev')
                                 <li class="current-menu-parent"><a href="/" title="">Home</a></li>
                                 @else
                                 <li class="" data-ur={{Request::url()}}><a href="/" title="">Home</a></li>
                                 @endif
                                 @if(Request::url() === 'http://jetit.dev/about-us')
                                 <li class="current-menu-parent">{{HTML::linkRoute('about','About Us')}}</li>
                                 @else
                                 <li>{{HTML::linkRoute('about','About Us')}}</li>
                                 @endif
                                 @if(Request::url() === 'http://jetit.dev/deals')
                                 <li class="current-menu-parent">{{HTML::linkRoute('jetdeals','JetIt Deals')}}</li>
                                 @else
                                 <li>{{HTML::linkRoute('jetdeals','JetIt Deals')}}</li>
                                 @endif
                                 @if(Request::url() === 'http://jetit.dev/on-demand-charter')
                                 <li class="current-menu-parent">{{HTML::linkRoute('demand','On Demand Charter')}}</li>
                                 @else
                                 <li>{{HTML::linkRoute('demand','On Demand Charter')}}</li>
                                 @endif
                                 @if(Request::url() === 'http://jetit.dev/get-in-touch' || Request::url() === 'http://jetit.dev/grow-your-business')
                                 <li class="current-menu-parent">
                                    <a href="#">Contact Us</a>
                                    <ul class="sub-menu">
                                        <li>
                                            {{HTML::linkRoute('gettouch','Get In Touch')}}
                                        </li>
                                        <li>
                                            {{HTML::linkRoute('growbusiness','Grow Your Business')}}
                                        </li>
                                    </ul>
                                </li>
                                @else
                                <li class="menu-parent">
                                    <a href="#">Contact Us</a>
                                    <ul class="sub-menu">
                                        <li>
                                            {{HTML::linkRoute('gettouch','Get In Touch')}}
                                        </li>
                                        <li>
                                            {{HTML::linkRoute('growbusiness','Grow Your Business')}}
                                        </li>
                                    </ul>
                                </li>
                                 <li>{{HTML::linkRoute('admin','Admin')}}</li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
            <!--End Navigation-->
        </div>
    </header>
    <!-- End Header -->


    @yield('content')
    

    <footer>
        <div class="container">
            <div class="row">
                <!-- Logo -->
                <div class="col-md-4">
                    <div class="">
                        <a href="/" title=""><img src="{{URL::asset('images/logo1.png')}}" alt=""></a>
                    </div>
                </div>
                <div class="col-md-5">
                  <div class="follow-us">
                      <h4>Follow us</h4>
                      <div class="follow-group">
                          <a href="https://www.facebook.com/jetitaviation?fref=ts " title=""><i class="fa fa-facebook"></i></a>
                          <a href="https://twitter.com/jetitaviation" title=""><i class="fa fa-twitter"></i></a>
                          <a href="#" title=""><i class="fa fa-linkedin"></i></a>
                          <a href="https://instagram.com/jetit.in" title=""><i class="fa fa-instagram"></i></a>
                          <a href="https://plus.google.com/u/0/b/103849491797924482262/103849491797924482262/about/p/pub" title=""><i class="fa fa-google-plus"></i></a>

                      </div>
                  </div>
              </div>
              <!-- End Logo -->
              <!-- End Navigation Footer -->
              <!-- Footer Currency, Language -->
              <div class="col-sm-6 col-md-3">
                <p class="copyright">
                    © 2015 JetIt. All rights reserved.
                </p>
                <!--CopyRight-->
            </div>
            <!-- End Footer Currency, Language -->
        </div>
    </div>
</footer>

</div>


</body>
</html>
