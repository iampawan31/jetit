@extends('layouts.master')
@section('content')
<section class="sub-banner" style="margin-top: 82px;">
	<!--Background-->
	<div class="bg-parallax bg-1" style="background-position: 50% 8px;"></div>
	<!--End Background-->
	<!-- Logo -->
	<div class="logo-banner text-center" style="display: none;">
		<a href="" title="">
			<img src="images/logo-banner.png" alt="">
		</a>
	</div>
	<!-- Logo -->
</section>
<div class="main">
	<div class="container">
		<div class="main-cn bg-white clearfix">
			<!-- Breakcrumb -->
			<section class="breakcrumb-sc">
			<ul class="breadcrumb arrow">
					<li><a href="/"><i class="fa fa-home"></i></a></li>
					<li>Get In Touch</li>
				</ul>
			</section>
			<!-- End Breakcrumb -->
			<section class="contact-page">
				<div class="contact-maps" style="background-image: url('images/jet.jpg');">
					
				</div>
				<div class="contact-cn">
					<h2>We are always in touch</h2>
					<ul>
						<li>
							<img src="images/icon-maker-contact.png" alt="">
							25 California Avenue, Santa Monica, California.
						</li>
						<li>
							<img src="images/icon-phone.png" alt="">
							+1-888-8765-1234
						</li>
						<li>
							<img src="images/icon-email.png" alt="">
							<a href="">contact@jetit.com</a>
						</li>
					</ul>
					<div class="form-contact">
						<form id="contact-form" action="processContact.php" method="post" novalidate="novalidate">
							<div class="form-field">
								<input type="text" name="fname" id="name" class="field-input" placeholder="First Name">
							</div>
							<div class="form-field">
								<input type="text" name="lname" id="email" class="field-input" placeholder="Last Name">
							</div>
							<div class="form-field">
								<input type="text" name="phone" id="email" class="field-input" placeholder="Phone Number">
							</div>
							<div class="form-field">
								<input type="text" name="email" id="email" class="field-input" placeholder="Email">
							</div>
							<div class="form-field form-field-area">
								<textarea name="message" id="message" cols="30" rows="10" class="field-input" placeholder="Comments"></textarea>
							</div>
							<div class="form-field text-center">
								<button type="submit" id="submit-contact" class="awe-btn awe-btn-2 arrow-right arrow-white awe-btn-lager">Submit</button>
							</div>
							<div id="contact-content">
							</div>
						</form>
					</div>
				</div>
			</section>
		</div>
	</div>
</div>
@stop