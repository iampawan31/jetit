@extends('layouts.master')
@section('content')
<script>
	$("#datepicker11").click(function(){
		$( ".datepicker12" ).datepicker({
		dateFormat: "dd-mm-yy",
		minDate: new Date()
	});
    });
</script>	
<script type="text/javascript">

$(document).ready(function(){
	var count =1;
	var maxField = 10; //Input fields increment limitation
	var addButton = $('.add_button'); //Add button selector
	var wrapper = $('.field_wrapper'); //Input field wrapper
	var fieldHTML = '<div class="row"><div class="form-group col-xs-12 col-sm-12 col-md-6"><input type="text" name="flightlegfrom';
	var fieldHTML1 =  '" value="" class="form-control" placeholder="From"/></div><div class="form-group col-xs-12 col-sm-12 col-md-6"><input type="text" name="flightlegto';
	var fieldHTML2 = '" value="" class="form-control" id="flight-to4" placeholder="To"/></div></div><div class="row"><div class="form-group col-xs-12 col-sm-12 col-md-6"><input type="text" class="form-control datepicker12" id="datepicker11" name="departuredate';
	var fieldHTML3 =  '" value="" class="form-control" placeholder="Departure Date"/></div><div class="form-group col-xs-12 col-sm-12 col-md-6"><input type="text" name="departuretime';
	var fieldHTML4 = '" value="" class="form-control time1" placeholder="Departure Time"/></div></div>'; //New input field html 
	var x = 1; //Initial field counter is 1
	$(addButton).click(function(){ //Once add button is clicked
		if(x < maxField){ //Check maximum number of input fields
			x++; //Increment field counter
			$(wrapper).append(fieldHTML+count+fieldHTML1+count+fieldHTML2+count+fieldHTML3+count+fieldHTML4);
			count++; // Add field html
		}
	});
	$(wrapper).on('click', '.remove_button', function(e){ //Once remove button is clicked
		e.preventDefault();
		$(this).parent('div').remove(); //Remove field html
		x--; //Decrement field counter
	});
});
</script>
<script>
	$(document).ready(function() {
    

    $('.time').timepicker({
    	'showDuration': true,
    	'timeFormat': 'g:ia'
    });

    $( "#flight-from" ).autocomplete({
    	source: 'fetchairports',
    	autoFocus: true,
    	select: function(event, ui) {
    		$('#flight-from').val(ui.item.value);
    	},
    	minLength: 1
    });
    $( "#flight-to" ).autocomplete({
    	source: 'fetchairports',
    	autoFocus: true,
    	select: function(event, ui) {
    		$('#flight-to').val(ui.item.value);
    	},
    	minLength: 1
    });
     $( "#flight-from1" ).autocomplete({
    	source: 'fetchairports',
    	autoFocus: true,
    	select: function(event, ui) {
    		$('#flight-from').val(ui.item.value);
    	},
    	minLength: 1
    });
    $( "#flight-to1" ).autocomplete({
    	source: 'fetchairports',
    	autoFocus: true,
    	select: function(event, ui) {
    		$('#flight-to').val(ui.item.value);
    	},
    	minLength: 1
    });
    $( ".flightlegto1" ).autocomplete({
    	source: 'fetchairports',
    	autoFocus: true,
    	select: function(event, ui) {
    		$('.flightlegto1').val(ui.item.value);
    	},
    	minLength: 1
    });

});


//for ppoulating the values in the modal form

$("#slick-popup").click(function(){
	console.log("clicked");
});

$("form#dealsform").submit(function(event){
   event.preventDefault();
   if($("#flight-from1").val() == "" || $("#flight-to1").val() == ""){
       console.log("submit");
   }
});
</script>
<script>
	$(function() {
	$( "#datepicker11" ).datepicker({
	dateFormat: "dd-mm-yy",
	minDate: new Date()
	});
	});
	</script>

<style>
	.input-group
	{
		width: 100%;
	}
</style>
<!-- -Login Modal -->
<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content login-modal">
			<div class="modal-header login-modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title text-center" id="loginModalLabel">REQUEST A QUOTE</h4>
			</div>
			<div class="modal-body">
				<div class="text-center">
					<div role="tabpanel" class="login-tab">
						<!-- Nav tabs -->
						<ul class="nav nav-tabs" role="tablist">
							<li role="presentation" class="active"><a id="signin-taba" href="#home" aria-controls="home" role="tab" data-toggle="tab">Sign In</a></li>
							<li role="presentation"><a id="signup-taba" href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Sign Up</a></li>
							<li role="presentation"><a id="forgetpass-taba" href="#forget_password" aria-controls="forget_password" role="tab" data-toggle="tab">Forget Password</a></li>
						</ul>

						<!-- Tab panes -->
						<div class="tab-content">
							<div role="tabpanel" class="tab-pane active text-center" id="profile">

								&nbsp;&nbsp;
								<span id="registration_fail" class="response_error" style="display: none;">Registration failed, please try again.</span>
								<div class="clearfix"></div>
								<form action="insertreg" method="post">
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-6">

											<a href="javascript:;" class="signup-tab" style="padding: 6%;background-color: #FFFFFF;color: #473B3B;margin-top: -3%;margin-bottom: 4%;border: 1px rgba(0, 0, 0, 0.16) solid;">ONE WAY</a>
											
										</div>

										<div class="col-xs-12 col-sm-12 col-md-6">

											<a href="javascript:;" class="forgetpass-tab" style="padding: 6%;background-color: #FFFFFF;color: #473B3B;margin-top: -3%;margin-bottom: 4%;border: 1px rgba(0, 0, 0, 0.16) solid;">MULTI DESTINATION</a>
										</div>
									</div>
									<br>
									<hr>
									<div class="row">
										<div class="form-group col-xs-12 col-sm-12 col-md-6">
											<div class="input-group">

												<input type="text" class="form-control" name="from1" id="username" placeholder="From " required>
												<span class="warlock-error" id="firstnameerror">{{$errors->register->first('from1')}}</span>
											</div>
											<span class="help-block has-error" data-error='0' id="username-error"></span>
										</div>
										<div class="form-group col-xs-12 col-sm-12 col-md-6">
											<div class="input-group">

												<input type="text" class="form-control" name="to1" id="remail" placeholder="To" required>
												<span class="warlock-error" id="firstnameerror">{{$errors->register->first('to1')}}</span>
											</div>
											<span class="help-block has-error" data-error='0' id="remail-error"></span>
										</div>
									</div>
									<div class="row">

										<div class="form-group col-xs-12 col-sm-12 col-md-6">
											<script>
												$(function() {
													$( ".datepicker1" ).datepicker({
														dateFormat: "dd-mm-yy",
														minDate: new Date()
													});
												});
											</script>

											<div class="input-group">

												<input type="text" class="form-control datepicker1" id="" name="departuredate" placeholder="Departure Date">
											</div>
											<span class="help-block has-error" data-error='0' id="username-error"  placeholder="Departure Time"></span>
										</div>
										<div class="form-group col-xs-12 col-sm-12 col-md-6">
											<div class="input-group">
												<input class="timepicker time form-control" name="departuretime" id="departure_time" placeholder="Departure Time" required>
											</div>
											<span class="help-block has-error" data-error='0' id="remail-error"></span>
										</div>
									</div>
									
									<script>
                                        $(document).ready(function(){
                                            $("#button").click(function(){
                                            	$("#roundtrip-value").val("1");
                                                $(".buton").show();
                                               $("#ds").prop('required',true);
                                                $(this).hide();
                                            });
                                        });
                                        </script>
                                         <div class="row buton" style="display:none">
                                        <div class="form-group col-xs-12 col-sm-12 col-md-6">
                                            <script>
                                            $(function() {
                                              $( ".datepicker1" ).datepicker({
                                                dateFormat: "dd-mm-yy",
                                                minDate: new Date()
                                              });
                                            });
                                            </script>
                                                    
                                                <div class="input-group">
                                                
                                                    <input type="text" class="form-control datepicker1" id="ds" placeholder="Return Date"  name="returndate" >
                                                </div>
                                                <span class="help-block has-error" data-error='0' id="username-error" name="returndate" placeholder="Return Time"></span>
                                            </div>
                                            <div class="form-group col-xs-12 col-sm-12 col-md-6">
                                                <div class="input-group">
                                                <input class="timepicker time form-control" name="returntime" id="departure_time ds" placeholder="Return Time">
                                                <input type="hidden" id="roundtrip-value" name="roundtrip">
                                                </div>
                                                <span class="help-block has-error" data-error='0' id="remail-error"></span>
                                            </div>
                                        </div>
                                        <button class="add_field_button" id="button" style="padding: 2%;background-color: #E9AD41;color: #fff;margin-top: -3%;margin-bottom: 4%; margin-right: 39%; border: #fff;float: left;">Round Trip</button>
									<div class="row">
										<div class="form-group col-xs-12 col-sm-12 col-md-6">
											<div class="input-group">

												<input type="text" class="form-control" id="username" name="numberofpassengers" placeholder="Number Of Passangers" required>
											</div>
											<span class="help-block has-error" data-error='0' id="username-error"></span>
										</div>
										<div class="form-group col-xs-12 col-sm-12 col-md-6">
											<div class="input-group" style="width: 100%;">
												<select class="form-control" placeholder="Select a Jet Type" name="jettype" required>
													<option>Select a Jet Type</option>
													<option value"Light-Jet">Light-Jet</option>
													<option value="Mid-Size">Mid-Size</option>
													<option value="Super Mid-Size">Super Mid-Size</option>
													<option value="Large Cabin">Large Cabin</option>
													<option value="Extended Range">Extended Range</option>
													<option value="Turbo Prop">Turbo Prop</option>
													<option value="New Helicopter">New Helicopter</option>

												</select>
											</div>
											<span class="help-block has-error" data-error='0' id="remail-error"></span>
										</div>
									</div>
									<hr>
									<div class="row">
										<div class="form-group col-xs-12 col-sm-12 col-md-6">
											<div class="input-group">

												<input type="text" class="form-control" id="username" name="fistname" placeholder="First Name" required>
											</div>
											<span class="help-block has-error" data-error='0' id="username-error"></span>
										</div>
										<div class="form-group col-xs-12 col-sm-12 col-md-6">
											<div class="input-group">

												<input type="text" class="form-control" id="remail" name="lastname" placeholder="Last Name" required>
											</div>
											<span class="help-block has-error" data-error='0' id="remail-error"></span>
										</div>
									</div>
									<div class="row">
										<div class="form-group col-xs-12 col-sm-12 col-md-6">
											<div class="input-group">

												<input type="text" class="form-control" id="username" name="contact" placeholder="Phone Number" required>
											</div>
											<span class="help-block has-error" data-error='0' id="username-error"></span>
										</div>
										<div class="form-group col-xs-12 col-sm-12 col-md-6">
											<div class="input-group">

												<input type="text" class="form-control" id="remail" name="mail" placeholder="Email" required>
											</div>
											<span class="help-block has-error" data-error='0' id="remail-error"></span>
										</div>
									</div>
									<div class="row">
										<div class="form-group col-xs-12 col-sm-12 col-md-12">
											<div class="input-group">

												<textarea class="form-control" rows="5" name="comment" placeholder="comments"></textarea>
											</div>
											<span class="help-block has-error" data-error='0' id="username-error"></span>
										</div>

									</div>


									<button type="submit" id="register_btn" class="btn btn-block bt-login" data-loading-text="Registering...." style="border-radius: 1px;">Request a Quote</button>
									<div class="clearfix"></div>
									
								</form>
							</div>
							<div role="tabpanel" class="tab-pane text-center" id="forget_password">
								&nbsp;&nbsp;
								<span id="reset_fail" class="response_error" style="display: none;"></span>
								<div class="clearfix"></div>
								<form action="insertmultiway" method="post">
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-6">

											<a href="javascript:;" class="signup-tab" style="padding: 6%;background-color: #FFFFFF;color: #473B3B;margin-top: -3%;margin-bottom: 4%;border: 1px rgba(0, 0, 0, 0.16) solid;">ONE WAY</a>
											
										</div>

										<div class="col-xs-12 col-sm-12 col-md-6">

											<a href="javascript:;" class="forgetpass-tab" style="padding: 6%;background-color: #FFFFFF;color: #473B3B;margin-top: -3%;margin-bottom: 4%;border: 1px rgba(0, 0, 0, 0.16) solid;">MULTI DESTINATION</a>
										</div>
									</div>
									<br>
									<hr>
									<div class="row">
										<div class="form-group col-xs-12 col-sm-12 col-md-6">
											<div class="input-group">

												<input type="text" class="form-control" id="username" name="from2" placeholder="From" >
											</div>
											<span class="help-block has-error" data-error='0'  id="username-error"></span>
										</div>
										<div class="form-group col-xs-12 col-sm-12 col-md-6">
											<div class="input-group">

												<input type="text" class="form-control" id="remail" name="to2" placeholder="To" >
											</div>
											<span class="help-block has-error" data-error='0' id="remail-error"></span>
										</div>
									</div>
									<div class="row">
										<script>
											$(function() {
												$( "#datepicker2" ).datepicker({
													dateFormat: "dd-mm-yy",
													minDate: new Date()
												});
											});
										</script>
										<div class="form-group col-xs-12 col-sm-12 col-md-6">
											<div class="input-group">

												<input type="text" class="form-control" id="datepicker2" name="departuredate11" placeholder="Departure Date" >
											</div>
											<span class="help-block has-error" data-error='0' id="username-error"></span>
										</div>
										<div class="form-group col-xs-12 col-sm-12 col-md-6">
											<div class="input-group">

												<input type="text" class="form-control time" id="remail" name="departuretime11" placeholder="Departure Time" >
											</div>
											<span class="help-block has-error" data-error='0' id="remail-error"></span>
										</div>
									</div>
									<div class="field_wrapper">
										       
									 </div>
									 <a href="javascript:void(0);" class="add_button" title="Add field" style="padding: 2%;background-color: #E9AD41;color: #fff;margin-top: -3%;margin-bottom: 4%; margin-right: 39%; border: #fff;float: left;">ADD A LEG</a><a href="javascript:void(0);" class="add_button" title="Add field"></a>
									<div class="row">
										<div class="form-group col-xs-12 col-sm-12 col-md-6">
											<div class="input-group">

												<input type="text" class="form-control" id="username" name="numberofpassengers1" placeholder="Number Of Passangers" >
											</div>
											<span class="help-block has-error" data-error='0' id="username-error"></span>
										</div>
										<div class="form-group col-xs-12 col-sm-12 col-md-6">
											<div class="input-group" style="width: 100%;">
												<select class="form-control" name="jettype1" placeholder="Select a Jet Type" >
													<option>Select a Jet Type</option>
													<option value"Light-Jet">Light-Jet</option>
													<option value="Mid-Size">Mid-Size</option>
													<option value="Super Mid-Size">Super Mid-Size</option>
													<option value="Large Cabin">Large Cabin</option>
													<option value="Extended Range">Extended Range</option>
													<option value="Turbo Prop">Turbo Prop</option>
													<option value="New Helicopter">New Helicopter</option>

												</select>
											</div>
											<span class="help-block has-error" data-error='0' id="remail-error"></span>
										</div>
									</div>
									<hr>
									<div class="row">
										<div class="form-group col-xs-12 col-sm-12 col-md-6">
											<div class="input-group">

												<input type="text" class="form-control" id="username" name="fistname1" placeholder="First Name" >
											</div>
											<span class="help-block has-error" data-error='0' id="username-error"></span>
										</div>
										<div class="form-group col-xs-12 col-sm-12 col-md-6">
											<div class="input-group">

												<input type="text" class="form-control" id="remail" name="lastname1" placeholder="Last Name" >
											</div>
											<span class="help-block has-error" data-error='0' id="remail-error"></span>
										</div>
									</div>
									<div class="row">
										<div class="form-group col-xs-12 col-sm-12 col-md-6">
											<div class="input-group">

												<input type="text" class="form-control" id="username" name="contact1" placeholder="Phone Number" >
											</div>
											<span class="help-block has-error" data-error='0' id="username-error"></span>
										</div>
										<div class="form-group col-xs-12 col-sm-12 col-md-6">
											<div class="input-group">

												<input type="text" class="form-control" id="remail" name="mail1" placeholder="Email" >
											</div>
											<span class="help-block has-error" data-error='0' id="remail-error"></span>
										</div>
									</div>
									<div class="row">
										<div class="form-group col-xs-12 col-sm-12 col-md-12">
											<div class="input-group">

												<textarea class="form-control" rows="5" name="comment1" placeholder="comments" ></textarea>
											</div>
											<span class="help-block has-error" data-error='0' id="username-error"></span>
										</div>

									</div>


									<button type="submit" id="register_btn" class="btn btn-block bt-login" data-loading-text="Registering....">Request a Quote</button>
									<div class="clearfix"></div>
									
								</form>
							</div>
						</div>
					</div>

				</div>
			</div>

		</div>
	</div>
</div>
<!-- - Login Model Ends Here -->

<section class="banner">

	<!--Background-->
	<div class="bg-parallax bg-11"></div>
	<!--End Background-->

	<div class="container">


		<div class="logo-banner text-center" style="display: none;">
                    <a href="#" title="">
                        <img src="{{URL::asset('images/logo-banner.png')}}" alt="">
                    </a>
                </div>
		<!-- Banner Content -->
		<div class="banner-cn">

			<!-- Tabs Cat Form -->
			<ul class="tabs-cat text-center row">
				<li class="cate-item active col-xs-6">
					<a data-toggle="tab" href="#form-flight" title="">
						<span>Jetit Deals</span>

						<img src="images/icon-flight.png" alt="piyush">
					</a>
				</li>

				<li class="cate-item col-xs-6">
					<a data-toggle="tab" href="#form-hotel" title=""><span>On Demand Charter</span><img src="images/icon-hotel.png" alt=""></a>
				</li>


			</ul>
			<!-- End Tabs Cat -->

			<!-- Tabs Content -->
			<div class="tab-content">

				<!-- Search Hotel -->
				<div class="form-cn form-hotel tab-pane " id="form-hotel">
					<h2>Where would you like to go?</h2>
					<div class="form-search clearfix">
						<div class="form-field field-destination" style="width: 40%;">
							<input type="text" name="flightfrom" id="flight-from" class="field-input" placeholder="From">
						</div>
						<div class="form-field field-to" style="width: 40%;">
							<input type="text" id="flight-to" class="field-input" placeholder="To">
						</div>


						<div class="form-submit">
							<button id="#slick-popup" href="#slick-popup" type="submit" class="awe-btn awe-btn-lager awe-search"><a class="popup-button"  data-toggle="modal" data-target="#loginModal"> Search</a></button>
						</div>
					</div>
				</div>
				<!-- End Search Hotel -->




				<!-- Search Flight-->
				<div class="form-cn form-flight tab-pane active in" id="form-flight">
					<h2>Where would you like to go?</h2>
					<div class="form-search clearfix">
					<form action="deals" method="POST" id="dealsform">
						<div class="form-field field-destination">
							<input type="text" name="dealflightfrom" id="flight-from1" class="field-input" placeholder="From">
						</div>
						<div class="form-field field-to">
							<input type="text" id="flight-to1" name="dealflightto" class="field-input" placeholder="To">
						</div>

						<div class="form-field field-adult" style="width: 18%;">
								<input type="text" id="" name="dealdeparturedate" class="datepicker1 field-input" placeholder="Date">
						</div>
						<div class="form-field field-select field-adult" style="width: 18%;">
								<select class="field-input" name="dealflighttype">
									<option value="1">Any Aircraft</option>
									<option value="Turbo Prop">Turbo Prop</option>
									<option value="Light Jet">Light Jet</option>
									<option value="Mid Size">Mid Size</option>
									<option value="Super Mid Size">Super Mid Size</option>
									<option value="Long Range">Long Range</option>
									<option value="Turbo Prop">Turbo Prop</option>
									<option value="Helicopter">Helicopter</option>
								</select>
						</div>

						<div class="form-submit">
							<button id="modal_trigger"  type="submit" class="awe-btn awe-btn-lager awe-search" style="height: 46px;">Search</button>
						</div>
						</form>
					</div>
				</div>
				<!-- End Search Flight -->

				<!-- Search Package -->

				<!-- End Search Package -->

				<!-- Search Tour-->

				<!-- End Search Tour -->

			</div>
			<!-- End Tabs Content -->

		</div>
		<!-- End Banner Content -->

	</div>

</section>
<!--End Banner-->

<!-- Sales -->
<section class="sales">
	<!-- Title -->
	<div class="title-wrap">
		<div class="container">
			<div class="travel-title float-left">
				<h2>Hot Sale Today: <span>Paris, Amsterdam, Saint Petersburg & more</span></h2>
			</div>
			{{HTML::linkRoute('jetdeals','ALL DEALS',array(), array('class' => 'awe-btn awe-btn-5 awe-btn-lager arrow-right text-uppercase float-right'))}}
		</div>
	</div>
	
	<!-- End Title -->
	<!-- Hot Sales Content -->
	<div class="container">
		<div class="sales-cn">
		    <div class="row">  {{--Row Start--}}
                                            <?php $count = 0; ?>
                                            @foreach ($deals as $deal)
                                            @if ($count == 2)
                                            <div class="row">
                                                @endif
                                                <div class="col-xs-6 col-md-4">
                                                    <div class="sales-item">
                                                        <figure class="home-sales-img">
                                                            <a href="#" title="">
                                                                <img src="{{URL::asset($deal->cityimage)}}" onmouseover="this.src='{{URL::asset($deal->flightimage)}}'" onmouseout="this.src='{{URL::asset($deal->cityimage)}}'" style="height: 293px;" border="0" alt="">
                                                            </a>
                                                            <figcaption>
                                                            Save<span><br>{{$deal->discount}}</span>%
                                                            </figcaption>
                                                        </figure>
                                                        <div class="home-sales-text">
                                                            <div class="home-sales-name-places">
                                                                <div class="home-sales-name">
                                                                    <a href="#" title="">{{$deal->flightfrom}}</a>
                                                                </div>
                                                                <div class="home-sales-places">
                                                                    <a href="#" title="">{{$deal->date}}</a>
                                                                </div>
                                                            </div>
                                                            @if ($deal->flighttype == 1)
                                                            International
                                                            @else
                                                            Domestic
                                                            @endif
                                                            <div class="price-box">
                                                                <span class="price old-price">From : {{$deal->flightto}}</span>
                                                                <span class="price special-price">{{$deal->price}}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                @if ($count == 2)
                                            </div>
                                            @endif
                                            <?php $count++;?>
                                            @endforeach
                                            <!-- HostSales Item -->
                                            
                                            
                                        </div>
		
		</div>
	</div>
	<!-- End Hot Sales Content -->
</section>
<!-- End Sales -->

<!-- Travel Destinations -->

<!-- End Travel Destinations -->

<!-- Travel Magazine -->


<!-- Confidence and Subscribe  -->
<section class="confidence-subscribe">
	<!-- Background -->
	<div class="bg-parallax bg-3"></div>
	<!-- End Background -->
	<div class="container">
		<div class="row " style="margin-top: 12%; margin-bottom: 11%;">
			<!-- Confidence -->
			<div class="col-md-6">
				<div class="confidence">
					<h3>Subscribe to our newsletter</h3>
					<h3 style="color:#fff; border: none;">Enter your email address and we’ll send you our regular promotional emails, packed with special offers, great deals, and huge discounts</h3>
				</div>
			</div>
			<!-- End Confidence -->
			<!-- Subscribe -->
			<div class="col-md-6">
				<div class="subscribe">

					<!-- Subscribe Form -->
					<div class="subscribe-form" style="margin-top: 21%;">
						<form action="#" method="get">
							<input type="text" name="" value="" placeholder="Your email" class="subscribe-input">
							<button type="submit" class="awe-btn awe-btn-5 arrow-right text-uppercase awe-btn-lager">SUBSCRIBE</button>
						</form>
					</div>
					<!-- End Subscribe Form -->
					<!-- Follow us -->

					<!-- Follow us -->
				</div>
			</div>
			<!-- End Subscribe -->

		</div>
	</div>
</section>

@stop
