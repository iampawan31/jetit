@extends('layouts.master')
@section('content')

<section class="sub-banner" style="margin-top: 82px;">
	<!--Background-->
	<div class="bg-parallax bg-1" style="background-position: 50% 2px;"></div>
	<!--End Background-->
	<!-- Logo -->
	<div class="logo-banner text-center" style="display: none;">
		<a href="" title="">
			<img src="images/logo-banner.png" alt="">
		</a>
	</div>
	<!-- Logo -->
</section>
<div class="main contact-fix" style="padding-bottom: 1% !important; ">
	<div class="container">
		<div class="main-cn about-page bg-white clearfix">

			<!-- Breakcrumb -->
			<section class="breakcrumb-sc">
				<ul class="breadcrumb arrow" style="padding:0px; margin-bottom: 0px;">
					<li><a href="/"><i class="fa fa-home"></i></a></li>
					<li>About us</li>
				</ul>
			</section>
			<!-- End Breakcrumb -->
			<!-- About -->
			<section  class="about-cn clearfix">
			<div class="element-cn">
                            
                            <ul class="tabs-head nav-tabs-two">
                                <li class="active"><a data-toggle="tab" href="#section4" aria-expanded="true">ABOUT US</a></li>
                                <li class=""><a data-toggle="tab" href="#section5" aria-expanded="false">OUR EDGE</a></li>
                                <li><a data-toggle="tab" href="#section6">EMPTY LEGS</a></li>
                                
                            </ul>

                            <div class="tab-content">
                                <div id="section4" class="tab-pane fade active in">
                                    <p><br>
                                        JET-IT is an enterprise that lets you fly the way YOU want to fly and it is cheaper than you think. We provide aircraft for all travel sectors, anytime, anywhere and the way you want it.</p><p>
JET-IT proves that air charter need not be the exclusive preserve of tycoons, film stars or sports celebrities. Anybody can travel by air charter at a very reasonable cost, but it takes intelligent planning. This is where JET-IT comes into the picture. JET-IT has <b>round the clock liaison</b> with world wide operators and ensures that a global fleet is ready for our clients at any time. Our strong database has a<b> minute-to-minute access</b> to the very latest information on aircraft availability and operating conditions, offering you a choice of cost efficient options.</p><p>
JET-IT is all about your choice and control over your journey. We can tailor flight solutions to your special requirements. 
                                    </p>
                                </div>
                                <div id="section5" class="tab-pane fade">
                                    <p><br>
                                        JET-IT provides you with a <b>24-hour service team</b> all year long, 24/7/365, to make sure that your experience of air chartering is immaculate and seamless. The use of our cutting-edge flight tracking and reservation system enables us to deliver all patron necessities such as airport assistance, ground transport, in-flight catering or security services.  
Safety and security for you is our top most priority. We employ extremely rigorous procedures to vet our operators and charters to ensure all regulatory needs to be met and paperwork to be precise.
                                    </p>
                                </div>
                                <div id="section6" class="tab-pane fade">
                                    <p><br>
                                        For travellers who wish to experience the <b>luxury and comfort of chartered flights at a fraction of the cost</b> of a conventional private jet charter. JET-IT DEALS offer an aviation experience like no other ! 
Organized by a dedicated team of experts who optimally utilise the air charters that return empty on the way back from their initially defined destinations.  <br>
You have the advantages of :<br><ul>
<li>Massive savings – up to 75% off the price of a conventional charter flight <br></li>   
<li>Aircraft – a broad range of aircrafts and helicopters to choose from<br></li>
<li>Current information – constant update of the latest JET-IT Deals<br></li>
<li>Specialist staff – our Dedicated team is available 24/7 for quick consultation and requests.<br></li>
                                    </ul> </p>
                                </div>
                            </div>

                        </div>
                        </section>
                        <section  class="about-cn clearfix">
                        <h1>JETS</h1> 
            <div class="element-cn">
                            
                            <ul class="tabs-head nav-tabs-two">
                                <li class="active"><a data-toggle="tab" href="#section8" aria-expanded="true">Turbo Prop</a></li>
                                <li class=""><a data-toggle="tab" href="#section9" aria-expanded="false">VERY Light Jet</a></li>
                                <li><a data-toggle="tab" href="#section10">LIGHT</a></li>
                                <li><a data-toggle="tab" href="#section11">Mid Size</a></li>
                                <li><a data-toggle="tab" href="#section12">LARGE SIZE</a></li>
                                <li><a data-toggle="tab" href="#section13">Long Range</a></li>
                                 <li><a data-toggle="tab" href="#section14">Helicopter</a></li>
                            </ul><br><br>


                           

                        </div>
                            <div class="tab-content">
                                <div id="section8" class="tab-pane fade active in">
                                   <img src="images/turboprop.jpg" alt="">
                                </div>
                                <div id="section9" class="tab-pane fade">
                                    <img src="images/verylight.jpg" alt="">
                                   
                                </div>
                                <div id="section10" class="tab-pane fade">
                                   <img src="images/light.jpg" alt="">
                                  
                                </div>
                                <div id="section11" class="tab-pane fade">
                                    <img src="images/midsize.jpg" alt="">
                                </div>
                                <div id="section12" class="tab-pane fade">
                                    <img src="images/large.jpg" alt="">
                                </div>
                                <div id="section13" class="tab-pane fade">
                                    <img src="images/longrange.jpg" alt="">
                                </div>
                                <div id="section14" class="tab-pane fade">
                                    <img src="images/helicopter.jpg" alt="">
                                </div>
                            </div>
                        </section>
			
			<!-- End About -->
			

		</div>
	</div>
</div>
@stop