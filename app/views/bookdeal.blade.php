@extends('layouts.master')
@section('content')
<style>
.dotted
{
      border-left: 1px #ccc solid;
    border-left-style: dotted;
        margin-bottom: 2%;
}
.price ins {
    color: #e9ad41;
    font-size: 38px;
    font-weight: 400;
    text-decoration: none;
    font-family: Lato;
    border-bottom: 1px #ccc solid;
    border-bottom-style: dotted;
    padding-bottom: 1.5%;
}
.price.night {
    margin-top: 5px;
    padding-top: 10px;
   
    line-height: 20px;
}
.price {
    color: #666;
    display: block;
    line-height: 32px;
    font-family: 'Open sans';
    font-size: 14px;
    font-weight: 300;
}
.help-tip{
  position: absolute;
  top: 18px;
  right: 18px;
  text-align: center;
  background-color: #e9ad41;
  border-radius: 50%;
  width: 24px;
  height: 24px;
  font-size: 14px;
  line-height: 26px;
  cursor: default;
  margin-top: 8%;
}

.help-tip:before{
  content:'?';
  font-weight: bold;
  color:#fff;
}

.help-tip:hover p{
  display:block;
  transform-origin: 100% 0%;

  -webkit-animation: fadeIn 0.3s ease-in-out;
  animation: fadeIn 0.3s ease-in-out;

}

.help-tip p{  /* The tooltip */
  display: none;
  text-align: left;
  background-color: #1E2021;
  padding: 20px;
  width: 300px;
  position: absolute;
  border-radius: 3px;
  box-shadow: 1px 1px 1px rgba(0, 0, 0, 0.2);
  right: -4px;
  color: #FFF;
  font-size: 13px;
  line-height: 1.4;
}

.help-tip p:before{ /* The pointer of the tooltip */
  position: absolute;
  content: '';
  width:0;
  height: 0;
  border:6px solid transparent;
  border-bottom-color:#1E2021;
  right:10px;
  top:-12px;
}

.help-tip p:after{ /* Prevents the tooltip from being hidden */
  width:100%;
  height:40px;
  content:'';
  position: absolute;
  top:-40px;
  left:0;
}

/* CSS animation */

@-webkit-keyframes fadeIn {
  0% { 
    opacity:0; 
    transform: scale(0.6);
  }

  100% {
    opacity:100%;
    transform: scale(1);
  }
}

@keyframes fadeIn {
  0% { opacity:0; }
  100% { opacity:100%; }
}
</style>
<!-- -Login Modal -->
<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
  <div class="modal-dialog" style="margin-top: 15%;">
      <div class="modal-content login-modal">
          <div class="modal-header login-modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title text-center" id="loginModalLabel">BOOK THE DEAL</h4>
          </div>
          <div class="modal-body">
            <div class="text-center">
              <div role="tabpanel" class="login-tab">
              <!-- Nav tabs -->
              <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a id="signin-taba" href="#home" aria-controls="home" role="tab" data-toggle="tab">Sign In</a></li>
                <li role="presentation"><a id="signup-taba" href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Sign Up</a></li>
                <li role="presentation"><a id="forgetpass-taba" href="#forget_password" aria-controls="forget_password" role="tab" data-toggle="tab">Forget Password</a></li>
              </ul>
          
              <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active text-center" id="home">
                  &nbsp;&nbsp;
                  <span id="login_fail" class="response_error" style="display: none;">Loggin failed, please try again.</span>
                  <div class="clearfix"></div>
                  <form>
                  <div class="form-group">
                      <div class="input-group">
                          <div class="input-group-addon"><i class="fa fa-user"></i></div>
                          <input type="text" class="form-control" id="login_username" placeholder="Name">
                      </div>
                      <span class="help-block has-error" id="email-error"></span>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                          <div class="input-group-addon"><i class="fa fa-phone"></i></div>
                          <input type="text" class="form-control" id="password" placeholder="Contact Number">
                      </div>
                      <span class="help-block has-error" id="password-error"></span>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                          <div class="input-group-addon"><i class="fa fa-email"></i></div>
                          <input type="email" class="form-control" id="password" placeholder="Email">
                      </div>
                      <span class="help-block has-error" id="password-error"></span>
                    </div>
                    <button type="button" id="login_btn" class="btn btn-block bt-login" data-loading-text="Signing In....">Book Now</button>
                    <div class="clearfix"></div>
                    
                </form>
                </div>
                <div role="tabpanel" class="tab-pane" id="profile">
                    &nbsp;&nbsp;
                    <span id="registration_fail" class="response_error" style="display: none;">Registration failed, please try again.</span>
                  <div class="clearfix"></div>
                  <form>
                  <div class="form-group">
                      <div class="input-group">
                          <div class="input-group-addon"><i class="fa fa-user"></i></div>
                          <input type="text" class="form-control" id="username" placeholder="Username">
                      </div>
                      <span class="help-block has-error" data-error='0' id="username-error"></span>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                          <div class="input-group-addon"><i class="fa fa-at"></i></div>
                          <input type="text" class="form-control" id="remail" placeholder="Email">
                      </div>
                      <span class="help-block has-error" data-error='0' id="remail-error"></span>
                    </div>
                    <button type="button" id="register_btn" class="btn btn-block bt-login" data-loading-text="Registering....">Register</button>
                  <div class="clearfix"></div>
                  <div class="login-modal-footer">
                      <div class="row">
                      <div class="col-xs-8 col-sm-8 col-md-8">
                        <i class="fa fa-lock"></i>
                        <a href="javascript:;" class="forgetpass-tab"> Forgot password? </a>
                      
                      </div>
                      
                      <div class="col-xs-4 col-sm-4 col-md-4">
                        <i class="fa fa-check"></i>
                        <a href="javascript:;" class="signin-tab"> Sign In </a>
                      </div>
                    </div>
                    </div>
                </form>
                </div>
                <div role="tabpanel" class="tab-pane text-center" id="forget_password">
                  &nbsp;&nbsp;
                    <span id="reset_fail" class="response_error" style="display: none;"></span>
                    <div class="clearfix"></div>
                    <form>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-user"></i></div>
                            <input type="text" class="form-control" id="femail" placeholder="Email">
                        </div>
                        <span class="help-block has-error" data-error='0' id="femail-error"></span>
                      </div>
                      
                      <button type="button" id="reset_btn" class="btn btn-block bt-login" data-loading-text="Please wait....">Forget Password</button>
                    <div class="clearfix"></div>
                    <div class="login-modal-footer">
                        <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6">
                          <i class="fa fa-lock"></i>
                          <a href="javascript:;" class="signin-tab"> Sign In </a>
                        
                        </div>
                        
                        <div class="col-xs-6 col-sm-6 col-md-6">
                          <i class="fa fa-check"></i>
                          <a href="javascript:;" class="signup-tab"> Sign Up </a>
                        </div>
                      </div>
                      </div>
                  </form>
                  </div>
                </div>
            </div>
                
              </div>
            </div>
            
        </div>
     </div>
  </div>
  <!-- - Login Model Ends Here -->
<section class="sub-banner" style="margin-top: 82px;"><div class="bg-parallax bg-1" style="background-position: 50% 8px;"></div><div class="logo-banner text-center" style="display: none;"><a href="" title=""><img src="images/logo-banner.png" alt=""></a></div></section>

<div class="main">
  <div class="container">
    <div class="main-cn cruise-page bg-white clearfix">
      <div class="row">
        <div class="col-lg-4 " style="padding: 3%;">
          <figure class="cruise-img"><a href="cruise-detail.html"><img src="{{URL::asset($deal->flightimage)}}" alt="" style="width: 350px;height: 278px;"></a></figure>
          <br>
               <button class="awe-btn awe-btn-lager" href="javascript:;" data-toggle="modal" data-target="#loginModal" style="background-color: #e9ad41;color: #fff; width: 100%;">BOOK NOW</button>
        </div>
         <div class="col-lg-8" style="padding: 3%;">
         <div class="col-lg-8 dotted ">
            <div class="price-box">
                <span class="price">From<br><ins>{{$deal->flightto}}</ins></span> 
                <span class="price night"><b>Date: </b>{{$deal->date}}</span>
            </div>
            </div>
            <div class="col-lg-4 dotted ">
            <div class="price-box">
                <span class="price">{{$deal->aircrafttype}}<br><figure class="cruise-img"><a href="cruise-detail.html"><img src="images/mid_size_jet.png" alt="" ></a></figure></span> 
                <span class="price night"><b>Aircraft: </b>{{$deal->aircraftname}}</span>
            </div>
            </div>
            <br>
        <div class="col-lg-12 dotted ">
            <div class="price-box">
                <span class="price">To<br><ins>{{$deal->flightfrom}}</ins></span> 
                <span class="price night"><b>Time: </b>{{$deal->flighttime}} hrs</span>
            </div>
        </div>
        <br>
        <div class="col-lg-4 dotted ">
            <div class="price-box">
                <span class="price">Price<br><ins><i class="fa fa-inr"></i> {{$deal->price}}<div class="help-tip">
  <p>Book the whole aircraft at this price.</p>
</div></ins></span> 
                <span class="price night"><b>Max Capacity: </b>{{$deal->capacity}}</span>
            </div>
        </div></div>
         
         <a > Launch Login Modal Popup</a>
         

         
      </div>
    </div>
  </div>
</div>
@stop

