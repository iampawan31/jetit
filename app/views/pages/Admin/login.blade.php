@extends('layouts.master')
@section('content')
<section class="sub-banner" style="margin-top: 82px;">
   <div class="bg-parallax bg-1" style="background-position: 50% 8px;"></div>
   <div class="logo-banner text-center" style="display: none;"><a href="" title=""><img src="images/logo-banner.png" alt=""></a></div>
</section>
<div class="main">
   <div class="container">
      <div class="main-cn element-page bg-white clearfix">
         <section class="user-profile">
            <div class="user-form user-signup">
               <div class="row">
                  <div class="col-md-4 col-md-offset-4">
                     <h2 class="user-profile__title">Log in</h2>
                     <form action="login" method="POST" class="form-horizontal form-column form-bordered">
                     <div class="field-input"><input type="text" name="username" class="input-text form-control" placeholder="username"></div>
                     <br>
                     <div class="field-input"><input type="text" name="password" class="input-text form-control" placeholder="Password"></div>
                     <br>
                     <div class="field-input"><button class="awe-btn awe-btn-1 awe-btn-medium">Sign in</button></div>
                     </form>
                  </div>
               </div>
            </div>
         </section>
      </div>
   </div>
</div>
@stop