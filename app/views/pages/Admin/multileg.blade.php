@extends('layouts.admin.master')
@section('content')
<div class="container-fluid">
	<div class="page-header">
		<div class="" style="text-align: -webkit-center;font-family: 'Open Sans', sans-serif;font-weight: 600;font-size: 20px;color: #e9ad41;">
			<span style="">{{$value = Session::pull('dealupdate', '')}}</span>
		</div>
	</div>

	<div class="row">
					<div class="col-sm-12">
						<div class="box box-color box-bordered">
							<div class="box-title">
								<h3>
									<i class="fa fa-table"></i>
									Deal Log
								</h3>
							</div>
							<div class="box-content nopadding">
								<table class="table table-hover table-nomargin">
									<thead>
										<tr>
											<th>From</th>
											<th>To</th>
											<th>Departure Date</th>
											<th>Name</th>
											<th>Contact No</th>
											<th>Email</th>
											<th>Date Of Register</th>
											<th>Status</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
									@foreach ($multiways as $multiway)
										
										<tr>

											<td>{{$multiway->flightfrom}}</td>
											<td>{{$multiway->flightto}}</td>
											<td>{{$multiway->departuredate}}</td>
											<td>{{$multiway->firstname}}<br>{{$multiway->lastname}}</td>
											<td>{{$multiway->lastname}}</td>
											<td>{{$multiway->email}}</td>
											<td>{{$multiway->dateofregister}}</td>
											<td><input type="text" name="status"></td>
											<td><a href="#">Delete</a></td>
										</tr>
									@endforeach
									</tbody>
								</table>
								
								<div class="table-pagination">
									{{ $multiway->links() }}
								</div>
							</div>
						</div>
					</div>
				</div>
</div>
</div>
<script>
	$( "#flight-from" ).autocomplete({
		source: '../fetchairports',
		autoFocus: true,
		select: function(event, ui) {
			$('#flight-from').val(ui.item.value);
		},
		minLength: 1
	});
	$( "#flight-to" ).autocomplete({
		source: '../fetchairports',
		autoFocus: true,
		select: function(event, ui) {
			$('#flight-to').val(ui.item.value);
		},
		minLength: 1
	});
</script>
<script>
	$(function() {
		$( ".datepicker1" ).datepicker({
			dateFormat: "dd-mm-yy",
			minDate: new Date()
		});
	});
	$('.time').timepicker({
		'showDuration': true,
		'timeFormat': 'g:ia'
	});
</script>
@stop