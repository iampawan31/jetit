@extends('layouts.admin.master')
@section('content')
<div class="container-fluid">
	<div class="page-header">
		<div class="" style="text-align: -webkit-center;font-family: 'Open Sans', sans-serif;font-weight: 600;font-size: 20px;color: #e9ad41;">
			<span style="">{{$value = Session::pull('dealupdate', '')}}</span>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12">
			<div class="box box-color box-bordered">
				<div class="box-title">
					<h3>
						<i class="fa fa-table"></i>
						Deal Log
					</h3>
				</div>
				<div class="box-content nopadding">
					<table class="table table-hover table-nomargin">
						<thead>
							<tr>
								<th>From</th>
								<th>To</th>
								<th>Flight Date</th>
								<th>Flight Time</th>
								<th>Aircraft Name</th>
								<th>Price</th>
								<th>Date Of Post</th>
								<th>Edit</th>
								<th>Delete</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($deals as $deal)

							<tr>

								<td>{{$deal->flightfrom}}</td>
								<td>{{$deal->flightto}}</td>
								<td>{{$deal->date}}</td>
								<td>{{$deal->flighttime}}</td>
								<td>{{$deal->aircraftname}}</td>
								<td>{{$deal->price}}</td>
								<td>{{$deal->dateofpost}}</td>
								<td><a class="btn btn-small btn-info" href="{{ URL::to('crud/' . $deal->id . '/edit') }}">Edit</a></td>
								<td> {{ Form::open(array('url' => 'crud/' . $deal->id)) }}
									{{ Form::hidden('_method', 'DELETE') }}
									{{ Form::submit('Delete', array('class' => 'btn btn-warning')) }}
									{{ Form::close() }}</td>
								</tr>
								@endforeach
							</tbody>
						</table>

						<div class="table-pagination">
							{{ $deals->links() }}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$( "#flight-from" ).autocomplete({
		source: '../fetchairports',
		autoFocus: true,
		select: function(event, ui) {
			$('#flight-from').val(ui.item.value);
		},
		minLength: 1
	});
	$( "#flight-to" ).autocomplete({
		source: '../fetchairports',
		autoFocus: true,
		select: function(event, ui) {
			$('#flight-to').val(ui.item.value);
		},
		minLength: 1
	});
</script>
<script>
	$(function() {
		$( ".datepicker1" ).datepicker({
			dateFormat: "dd-mm-yy",
			minDate: new Date()
		});
	});
	$('.time').timepicker({
		'showDuration': true,
		'timeFormat': 'g:ia'
	});
</script>
@stop