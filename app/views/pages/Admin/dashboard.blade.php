@extends('layouts.admin.master')
@section('content')
<div class="container-fluid">
	<div class="page-header">
		<div class="" style="text-align: -webkit-center;font-family: 'Open Sans', sans-serif;font-weight: 600;font-size: 20px;color: #e9ad41;">
			<span style="">{{$value = Session::pull('dealupdate', '')}}</span>
		</div>
	</div>
	<div class="row">
		<div class="col-md-10 col-md-offset-1" style="box-shadow: 0px 1px 0px 7px #888888;">
			<div class="box" style="padding-bottom: 10px;">
				<div class="box-title">
					<h3>
					<i class="fa fa-list"></i>Add a Deal</h3>
				</div>
				<div class="box-content nopadding">
					<form action="insertdeal" method="POST" class="form-horizontal form-column form-bordered" enctype="multipart/form-data">
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<label for="textfield" class="control-label col-sm-2">From</label>
									<div class="col-sm-10">
										<input type="text" name="flighfrom" id="flight-from" placeholder="From" class="form-control">
										<span class="warlock-error" id="firstnameerror">{{$errors->deal->first('flighfrom')}}</span>
									</div>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label for="textfield" class="control-label col-sm-2">To</label>
									<div class="col-sm-10">
										<input type="text" name="flighto" id="flight-to" placeholder="To" class="form-control">
										<span class="warlock-error" id="firstnameerror">{{$errors->deal->first('flighto')}}</span>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<label for="textfield" class="control-label col-sm-2">Date</label>
									<div class="col-sm-10">
										<input type="text" name="departuredate"  class="form-control datepicker1" placeholder="Departure Date">
										<span class="warlock-error" id="firstnameerror">{{$errors->deal->first('departuredate')}}</span>
									</div>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label for="textfield" class="control-label col-sm-2">Time</label>
									<div class="col-sm-10">
										<input type="text" name="departuretime" id="textfield" placeholder="Departure Time" class="form-control time">
										<span class="warlock-error" id="firstnameerror">{{$errors->deal->first('departuretime')}}</span>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<label for="textfield" class="control-label col-sm-2">Passengers</label>
									<div class="col-sm-10">
										<input type="text" name="numberofpassengers" id="numberofpassengers" placeholder="Number of Passengers" class="form-control">
										<span class="warlock-error" id="firstnameerror">{{$errors->deal->first('numberofpassengers')}}</span>
									</div>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label for="textfield" class="control-label col-sm-2">Price</label>
									<div class="col-sm-10">
										<input type="text" name="price" id="textfield" placeholder="Price" class="form-control">
										<span class="warlock-error" id="firstnameerror">{{$errors->deal->first('price')}}</span>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<label for="textfield" class="control-label col-sm-2">Expiry date</label>
									<div class="col-sm-10">
										<select id="expirydate" name="expirydate" class="form-control">
											<option value="1" selected="true">1</option>
											<option value="2">2</option>
											<option value="3">3</option>
											<option value="4">4</option>
											<option value="5">5</option>
											<option value="6">6</option>
											<option value="8">8</option>
											<option value="9">9</option>
											<option value="10">10</option>
											<option value="11">11</option>
											<option value="12">12</option>
											<option value="13">13</option>
											<option value="14">14</option>
											<option value="15">15</option>
											<option value="16">16</option>
											<option value="17">17</option>
											<option value="18">18</option>
											<option value="19">19</option>
											<option value="20">20</option>
											<option value="21">21</option>
											<option value="22">22</option>
											<option value="23">23</option>
											<option value="24">24</option>
											<option value="25">25</option>
											<option value="26">26</option>
											<option value="27">27</option>
											<option value="28">28</option>
											<option value="29">29</option>
											<option value="30">30</option>
										</select>
										<span class="warlock-error" id="firstnameerror">{{$errors->deal->first('expirydate')}}</span>
									</div>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group" style="border-bottom: 1px solid #ccc;">
									<label for="textfield" class="control-label col-sm-2">Discount</label>
									<div class="col-sm-10">
										<input type="text" name="discount" id="textfield" placeholder="Discount" class="form-control">
										<span class="warlock-error" id="firstnameerror">{{$errors->deal->first('discount')}}</span>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<label for="password" class="control-label col-sm-2">Main Image</label>
									<div class="col-sm-10">
										<input type="file" name="mainimage" id="mainimage" class="form-control">
										<span class="warlock-error" id="firstnameerror">{{$errors->deal->first('mainimage')}}</span>
									</div>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label for="textfield" class="control-label col-sm-2">Hover Image</label>
									<div class="col-sm-10">
										<input type="file" name="hoverimage" id="hoverimage" class="form-control">
										<span class="warlock-error" id="firstnameerror">{{$errors->deal->first('hoverimage')}}</span>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<label for="password" class="control-label col-sm-2">Aircraft Type</label>
									<div class="col-sm-10">
										<select class="field-input form-control" name="aircrafttype">
											<option value="Turbo Prop">Turbo Prop</option>
											<option value="Light Jet">Light Jet</option>
											<option value="Mid Size">Mid Size</option>
											<option value="Super Mid Size">Super Mid Size</option>
											<option value="Long Range">Long Range</option>
											<option value="Helicopter">Helicopter</option>
										</select>
										<span class="warlock-error" id="firstnameerror">{{$errors->deal->first('aircrafttype')}}</span>
									</div>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label for="textfield" class="control-label col-sm-2">Aircraft Name</label>
									<div class="col-sm-10">
										<input type="text" name="aircraftname" id="textfield" placeholder="Aircraft Name" class="form-control">
										<span class="warlock-error" id="firstnameerror">{{$errors->deal->first('aircraftname')}}</span>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<label for="password" class="control-label col-sm-2">Flight Type</label>
									<div class="col-sm-10">
										<select name="flighttype" id="select" class="form-control">
											<option value="1">International</option>
											<option value="0">Domestic</option>
										</select>
										<span class="warlock-error" id="firstnameerror">{{$errors->deal->first('flighttype')}}</span>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="form-actions" style="text-align:-webkit-center;">
									<button type="submit" class="btn btn-primary">Add Deal</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
</div>
<script>
	$( "#flight-from" ).autocomplete({
		source: '../fetchairports',
		autoFocus: true,
		select: function(event, ui) {
			$('#flight-from').val(ui.item.value);
		},
		minLength: 1
	});
	$( "#flight-to" ).autocomplete({
		source: '../fetchairports',
		autoFocus: true,
		select: function(event, ui) {
			$('#flight-to').val(ui.item.value);
		},
		minLength: 1
	});
</script>
<script>
	$(function() {
		$( ".datepicker1" ).datepicker({
			dateFormat: "dd-mm-yy",
			minDate: new Date()
		});
	});
	$('.time').timepicker({
		'showDuration': true,
		'timeFormat': 'g:ia'
	});
</script>
@stop