@extends('layouts.master')
@section('content')
<script>
      $(document).ready(function() {
    var max_fields      = 11; //maximum input boxes allowed
    var wrapper2         = $(".input_fields_wrap2"); //Fields wrapper
    var wrapper3         = $(".input_fields_wrap3"); //Fields wrapper
    var wrapper4         = $(".input_fields_wrap4"); //Fields wrapper
    var wrapper5         = $(".input_fields_wrap5"); //Fields wrapper
    var add_button      = $(".add_field_buttons"); //Add button ID

    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper2).append('<div><input type="text" placeholder="From" class="form-control" name="mytext[]" style="margin-bottom: 5%;margin-top: 5%;"/></div>'); //add input box
            $(wrapper3).append('<div><input type="text" placeholder="To" name="mytext2[]" class="form-control" style="margin-bottom: 5%;margin-top: 5%;"/></div>'); //add input box
            $(wrapper4).append('<div><input type="text" placeholder="Departure Date" class="form-control" name="mytext[]" style="margin-bottom: 5%;margin-top: 5%;"/></div>'); //add input box
            $(wrapper5).append('<div><input type="text" placeholder="Departure Time" name="mytext2[]" class="form-control" style="margin-bottom: 5%;margin-top: 5%;"/></div>'); //add input box
            
        }
    });
 
     $('.time').timepicker({
        'showDuration': true,
        'timeFormat': 'g:ia'
    });

});
</script>
<style>
.input-group
{
    width: 100%;
}
</style>
<!-- -Login Modal -->
<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content login-modal">
            <div class="modal-header login-modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center" id="loginModalLabel">REQUEST A QUOTE</h4>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <div role="tabpanel" class="login-tab">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a id="signin-taba" href="#home" aria-controls="home" role="tab" data-toggle="tab">Sign In</a></li>
                            <li role="presentation"><a id="signup-taba" href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Sign Up</a></li>
                            <li role="presentation"><a id="forgetpass-taba" href="#forget_password" aria-controls="forget_password" role="tab" data-toggle="tab">Forget Password</a></li>
                        </ul>
                    
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active text-center" id="profile">
                                
                                &nbsp;&nbsp;
                                <span id="registration_fail" class="response_error" style="display: none;">Registration failed, please try again.</span>
                                <div class="clearfix"></div>
                                <form>
                                <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-6">
                                                
                                                <a href="javascript:;" class="signup-tab" style="padding: 6%;background-color: #FFFFFF;color: #473B3B;margin-top: -3%;margin-bottom: 4%;border: 1px rgba(0, 0, 0, 0.16) solid;">ONE WAY</a>
                                            
                                            </div>
                                            
                                            <div class="col-xs-12 col-sm-12 col-md-6">
                                                
                                                <a href="javascript:;" class="forgetpass-tab" style="padding: 6%;background-color: #FFFFFF;color: #473B3B;margin-top: -3%;margin-bottom: 4%;border: 1px rgba(0, 0, 0, 0.16) solid;">MULTI DESTINATION</a>
                                            </div>
                                        </div>
                                        <br>
                                        <hr>
                                        <div class="row">
                                            <div class="form-group col-xs-12 col-sm-12 col-md-6">
                                                <div class="input-group">
                                                    
                                                    <input type="text" class="form-control" id="username" placeholder="From ">
                                                </div>
                                                <span class="help-block has-error" data-error='0' id="username-error"></span>
                                            </div>
                                            <div class="form-group col-xs-12 col-sm-12 col-md-6">
                                                <div class="input-group">
                                                    
                                                    <input type="text" class="form-control" id="remail" placeholder="To">
                                                </div>
                                                <span class="help-block has-error" data-error='0' id="remail-error"></span>
                                            </div>
                                        </div>
                                        <div class="row">
                                        
                                            <div class="form-group col-xs-12 col-sm-12 col-md-6">
                                            <script>
                                            $(function() {
                                              $( ".datepicker1" ).datepicker({
                                                dateFormat: "DD, d MM, yy",
                                                minDate: new Date()
                                              });
                                            });
                                            </script>
                                                    
                                                <div class="input-group">
                                                
                                                    <input type="text" class="form-control datepicker1" id="" placeholder="Departure Date">
                                                </div>
                                                <span class="help-block has-error" data-error='0' id="username-error" placeholder="Departure Time"></span>
                                            </div>
                                            <div class="form-group col-xs-12 col-sm-12 col-md-6">
                                                <div class="input-group">
                                                <input class="timepicker time form-control" name="timepicker" id="departure_time" placeholder="Departure Time">
                                                </div>
                                                <span class="help-block has-error" data-error='0' id="remail-error"></span>
                                            </div>
                                        </div>
                                        <script>
                                        $(document).ready(function(){
                                            $("#button").click(function(){
                                                $(".buton").toggle();
                                            });
                                        });
                                        </script>
                                         <div class="row buton" style="display:none">
                                        <div class="form-group col-xs-12 col-sm-12 col-md-6">
                                            <script>
                                            $(function() {
                                              $( ".datepicker1" ).datepicker({
                                                dateFormat: "DD, d MM, yy",
                                                minDate: new Date()
                                              });
                                            });
                                            </script>
                                                    
                                                <div class="input-group">
                                                
                                                    <input type="text" class="form-control datepicker1" id="" placeholder="Return Date">
                                                </div>
                                                <span class="help-block has-error" data-error='0' id="username-error" placeholder="Return Time"></span>
                                            </div>
                                            <div class="form-group col-xs-12 col-sm-12 col-md-6">
                                                <div class="input-group">
                                                <input class="timepicker time form-control" name="timepicker" id="departure_time" placeholder="Return Time">
                                                </div>
                                                <span class="help-block has-error" data-error='0' id="remail-error"></span>
                                            </div>
                                        </div>
                                        <button class="add_field_button" id="button" style="padding: 2%;background-color: #E9AD41;color: #fff;margin-top: -3%;margin-bottom: 4%; margin-right: 39%; border: #fff;float: left;">Round Trip</button>
                                        <div class="row">
                                            <div class="form-group col-xs-12 col-sm-12 col-md-6">
                                                <div class="input-group">
                                                    
                                                    <input type="text" class="form-control" id="username" placeholder="Number Of Passangers">
                                                </div>
                                                <span class="help-block has-error" data-error='0' id="username-error"></span>
                                            </div>
                                            <div class="form-group col-xs-12 col-sm-12 col-md-6">
                                                <div class="input-group" style="width: 100%;">
                                                    <select class="form-control" placeholder="Select a Jet Type">
                                                    <option>Select a Jet Type</option>
                                                    <option value"Light-Jet">Light-Jet</option>
                                                    <option value="Mid-Size">Mid-Size</option>
                                                    <option value="Super Mid-Size">Super Mid-Size</option>
                                                    <option value="Large Cabin">Large Cabin</option>
                                                    <option value="Extended Range">Extended Range</option>
                                                    <option value="Turbo Prop">Turbo Prop</option>
                                                    <option value="New Helicopter">New Helicopter</option>

                                                    </select>
                                                </div>
                                                <span class="help-block has-error" data-error='0' id="remail-error"></span>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="form-group col-xs-12 col-sm-12 col-md-6">
                                                <div class="input-group">
                                                    
                                                    <input type="text" class="form-control" id="username" placeholder="First Name">
                                                </div>
                                                <span class="help-block has-error" data-error='0' id="username-error"></span>
                                            </div>
                                            <div class="form-group col-xs-12 col-sm-12 col-md-6">
                                                <div class="input-group">
                                                    
                                                    <input type="text" class="form-control" id="remail" placeholder="Last Name">
                                                </div>
                                                <span class="help-block has-error" data-error='0' id="remail-error"></span>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-xs-12 col-sm-12 col-md-6">
                                                <div class="input-group">
                                                    
                                                    <input type="text" class="form-control" id="username" placeholder="Phone Number">
                                                </div>
                                                <span class="help-block has-error" data-error='0' id="username-error"></span>
                                            </div>
                                            <div class="form-group col-xs-12 col-sm-12 col-md-6">
                                                <div class="input-group">
                                                    
                                                    <input type="text" class="form-control" id="remail" placeholder="Email">
                                                </div>
                                                <span class="help-block has-error" data-error='0' id="remail-error"></span>
                                            </div>
                                        </div>
                                        <div class="row">
                                        <div class="form-group col-xs-12 col-sm-12 col-md-12">
                                                <div class="input-group">
                                                    
                                                    <textarea class="form-control" rows="5" placeholder="comments"></textarea>
                                                </div>
                                                <span class="help-block has-error" data-error='0' id="username-error"></span>
                                            </div>

                                        </div>
                                    

                                    <button type="button" id="register_btn" class="btn btn-block bt-login" data-loading-text="Registering...." style="border-radius: 1px;">Register</button>
                                    <div class="clearfix"></div>
                                    
                                </form>
                            </div>
                            <div role="tabpanel" class="tab-pane text-center" id="forget_password">
                                &nbsp;&nbsp;
                                <span id="reset_fail" class="response_error" style="display: none;"></span>
                                    <div class="clearfix"></div>
                                    <form>
                                <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-6">
                                                
                                                <a href="javascript:;" class="signup-tab" style="padding: 6%;background-color: #FFFFFF;color: #473B3B;margin-top: -3%;margin-bottom: 4%;border: 1px rgba(0, 0, 0, 0.16) solid;">ONE WAY</a>
                                            
                                            </div>
                                            
                                            <div class="col-xs-12 col-sm-12 col-md-6">
                                                
                                                <a href="javascript:;" class="forgetpass-tab" style="padding: 6%;background-color: #FFFFFF;color: #473B3B;margin-top: -3%;margin-bottom: 4%;border: 1px rgba(0, 0, 0, 0.16) solid;">MULTI DESTINATION</a>
                                            </div>
                                        </div>
                                        <br>
                                        <hr>
                                        <div class="row">
                                            <div class="form-group col-xs-12 col-sm-12 col-md-6">
                                                <div class="input-group">
                                                    
                                                    <input type="text" class="form-control" id="username" placeholder="From">
                                                </div>
                                                <span class="help-block has-error" data-error='0' id="username-error"></span>
                                            </div>
                                            <div class="form-group col-xs-12 col-sm-12 col-md-6">
                                                <div class="input-group">
                                                    
                                                    <input type="text" class="form-control" id="remail" placeholder="To">
                                                </div>
                                                <span class="help-block has-error" data-error='0' id="remail-error"></span>
                                            </div>
                                        </div>
                                        <div class="row">
                                        <script>
                                            $(function() {
                                              $( "#datepicker2" ).datepicker({
                                                dateFormat: "DD, d MM, yy",
                                                minDate: new Date()
                                              });
                                            });
                                            </script>
                                            <div class="form-group col-xs-12 col-sm-12 col-md-6">
                                                <div class="input-group">
                                                    
                                                    <input type="text" class="form-control" id="datepicker2" placeholder="Departure Date">
                                                </div>
                                                <span class="help-block has-error" data-error='0' id="username-error"></span>
                                            </div>
                                            <div class="form-group col-xs-12 col-sm-12 col-md-6">
                                                <div class="input-group">
                                                    
                                                    <input type="text" class="form-control time" id="remail" placeholder="Departure Time">
                                                </div>
                                                <span class="help-block has-error" data-error='0' id="remail-error"></span>
                                            </div>
                                        </div>
                                         <div class="row">
                                        <div class="form-group col-xs-12 col-sm-12 col-md-6 input_fields_wrap2 field">
                                          <div class="input-group"> </div>
                                          <br>
                                        </div>
                                        <div class="form-group col-xs-12 col-sm-12 col-md-6 input_fields_wrap3 field">
                                          <div class="input-group"> </div>
                                          <br>
                                        </div>
                                        </div>
                                        <div class="row">
                                        <div class="form-group col-xs-12 col-sm-12 col-md-6 input_fields_wrap4 field">
                                          <div class="input-group"> </div>
                                          <br>
                                        </div>
                                        <div class="form-group col-xs-12 col-sm-12 col-md-6 input_fields_wrap5 field">
                                          <div class="input-group"> </div>
                                          <br>
                                        </div>
                                        </div>
                                       <button class="add_field_buttons " style="padding: 2%;background-color: #E9AD41;color: #fff;margin-top: -3%;margin-bottom: 4%; margin-right: 39%; border: #fff;float: left;">ADD A LEG</button>
                                        <div class="row">
                                            <div class="form-group col-xs-12 col-sm-12 col-md-6">
                                                <div class="input-group">
                                                    
                                                    <input type="text" class="form-control" id="username" placeholder="Number Of Passangers">
                                                </div>
                                                <span class="help-block has-error" data-error='0' id="username-error"></span>
                                            </div>
                                            <div class="form-group col-xs-12 col-sm-12 col-md-6">
                                                <div class="input-group" style="width: 100%;">
                                                    <select class="form-control" placeholder="Select a Jet Type">
                                                    <option>Select a Jet Type</option>
                                                    <option value"Light-Jet">Light-Jet</option>
                                                    <option value="Mid-Size">Mid-Size</option>
                                                    <option value="Super Mid-Size">Super Mid-Size</option>
                                                    <option value="Large Cabin">Large Cabin</option>
                                                    <option value="Extended Range">Extended Range</option>
                                                    <option value="Turbo Prop">Turbo Prop</option>
                                                    <option value="New Helicopter">New Helicopter</option>

                                                    </select>
                                                </div>
                                                <span class="help-block has-error" data-error='0' id="remail-error"></span>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="form-group col-xs-12 col-sm-12 col-md-6">
                                                <div class="input-group">
                                                    
                                                    <input type="text" class="form-control" id="username" placeholder="First Name">
                                                </div>
                                                <span class="help-block has-error" data-error='0' id="username-error"></span>
                                            </div>
                                            <div class="form-group col-xs-12 col-sm-12 col-md-6">
                                                <div class="input-group">
                                                    
                                                    <input type="text" class="form-control" id="remail" placeholder="Last Name">
                                                </div>
                                                <span class="help-block has-error" data-error='0' id="remail-error"></span>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-xs-12 col-sm-12 col-md-6">
                                                <div class="input-group">
                                                    
                                                    <input type="text" class="form-control" id="username" placeholder="Phone Number">
                                                </div>
                                                <span class="help-block has-error" data-error='0' id="username-error"></span>
                                            </div>
                                            <div class="form-group col-xs-12 col-sm-12 col-md-6">
                                                <div class="input-group">
                                                    
                                                    <input type="text" class="form-control" id="remail" placeholder="Email">
                                                </div>
                                                <span class="help-block has-error" data-error='0' id="remail-error"></span>
                                            </div>
                                        </div>
                                        <div class="row">
                                        <div class="form-group col-xs-12 col-sm-12 col-md-12">
                                                <div class="input-group">
                                                    
                                                    <textarea class="form-control" rows="5" placeholder="comments"></textarea>
                                                </div>
                                                <span class="help-block has-error" data-error='0' id="username-error"></span>
                                            </div>

                                        </div>
                                    

                                    <button type="button" id="register_btn" class="btn btn-block bt-login" data-loading-text="Registering....">Request a Quote</button>
                                    <div class="clearfix"></div>
                                    
                                </form>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
                
            </div>
       </div>
    </div>

<section class="sub-banner" style="margin-top: 82px;">
            <!--Background-->
            <div class="bg-parallax bg-1" style="background-position: 50% 8px;"></div>
            <!--End Background-->
            <!-- Logo -->
            <div class="logo-banner text-center" style="display: none;">
                <a href="" title="">
                    <img src="images/logo-banner.png" alt="">
                </a>
            </div>
            <!-- Logo -->
        </section>
      <div class="main">
            <div class="container">
               <div class="main-cn about-page bg-white clearfix">

                    <!-- Breakcrumb -->
                    <section class="breakcrumb-sc">
                        <ul class="breadcrumb arrow">
                            <li><a href="/"><i class="fa fa-home"></i></a></li>
                            <li>On Demand Charter</li>
                        </ul>
                    </section>
                    <!-- End Breakcrumb -->
                    <!-- About -->
                    <section class="about-cn clearfix">
                        <div class="col-xs-6 col-md-4">
                            <div class="about-searved">
                            <span>Served over</span>
                            <ins>584,000</ins>
                            <span>people in 45 countries</span>
                        </div>
                        </div>
                        <div class="col-xs-6 col-md-8">
                        <div class="about-text">
                            <h1>We love travel</h1>
                            <div class="about-description">
                                <p>
                                   JET-IT takes your travel to a whole new level of through our bespoke services. 
Our Bespoke Travel creates, plans and customises the finest of details for your requests, wishes and demands. Each flight is custom-made to utmost exclusivity providing extreme effectiveness and worth. We arrange the jet you want, when you want and where you want to fly.</p>
                                <p> Spontaneous, adaptable and easy to arrange,<b> JET-IT Bespoke guarantees a smooth booking process with a pay-as-you-fly model providing you liberty and peace of mind.</b> </p><p>
We hand over the power of decision-making and convenience to our customers in order to empower them with fundamental control over their flight.  Your advantages are :<br><ul>
<li>No lengthy queues or waiting at airports<br></li>
<li>No public scrutiny or screening<br></li>
<li>No conformity to airline schedules<br></li>
                                <li>No pre-set catering menu</li></ul>
                                </p>
                                
                            </div>
                        </div>
                        <br>
                        <section >
                   <button id="#slick-popup" href="#slick-popup" type="submit" class="awe-btn awe-btn-5 arrow-right text-uppercase awe-btn-lager"><a class="popup-button"  data-toggle="modal" data-target="#loginModal" style="color:#000"> GET A QUOTE</a></button>
                
            </section>
                        </div>
                    </section>
                    <!-- End About -->




                </div>
            </div>
        </div>
  @stop