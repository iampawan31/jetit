@extends('layouts.master')
@section('content')
<section class="sub-banner" style="margin-top: 82px;">
    <!--Background-->
    <div class="bg-parallax bg-1" style="background-position: 50% 8px;"></div>
    <!--End Background-->
    <!-- Logo -->
    <div class="logo-banner text-center" style="display: none;">
        <a href="" title="">
            <img src="images/logo-banner.png" alt="">
        </a>
    </div>
    <!-- Logo -->
</section>
<div class="main">
    <div class="container">
        <div class="main-cn hotel-page bg-white">
            <div class="row">
                <!-- Hotel Right -->
                <div class="col-md-12">
                    <!-- Breakcrumb -->
                    <section class="breakcrumb-sc">
                        <ul class="breadcrumb arrow">
                            <li><a href="/"><i class="fa fa-home"></i></a></li>
                            <li>Jetit Deals</li>
                        </ul>
                    </section>
                    <!-- End Breakcrumb -->
                    <!-- Hotel List -->
                    <section  class="about-cn clearfix">
                        <div class="element-cn" >
                            <ul class="tabs-head nav-tabs-two">
                                <li class="active"><a data-toggle="tab" href="#section4" aria-expanded="true">All Flights </a></li>
                                <li class=""><a data-toggle="tab" href="#section5" aria-expanded="false">International</a></li>
                                <li class=""><a data-toggle="tab" href="#section6" aria-expanded="false">Domestic</a></li>
                            </ul>
                            <div class="tab-content">
                                <div id="section4" class="tab-pane fade active in">
                                    <div class="sales-cn" style="margin-top: 2%;">
                                        <div class="row">  {{--Row Start--}}
                                            <?php $count = 0; ?>
                                            @foreach ($deals as $deal)
                                            @if ($count == 2)
                                            <div class="row">
                                                @endif
                                                <div class="col-xs-6 col-md-4">
                                                    <div class="sales-item">
                                                        <figure class="home-sales-img">
                                                            <a href="{{ route('deal',$deal->id)}}" title="">
                                                                <img src="{{URL::asset($deal->cityimage)}}" onmouseover="this.src='{{URL::asset($deal->flightimage)}}'" onmouseout="this.src='{{URL::asset($deal->cityimage)}}'" style="height: 293px;" border="0" alt="">
                                                            </a>
                                                            <figcaption>
                                                            Save<span><br>{{$deal->discount}}</span>%
                                                            </figcaption>
                                                        </figure>
                                                        <div class="home-sales-text">
                                                            <div class="home-sales-name-places">
                                                                <div class="home-sales-name">
                                                                    <a href="{{ route('deal',$deal->id)}}" title="">{{$deal->flightfrom}}</a>
                                                                </div>
                                                                <div class="home-sales-places">
                                                                    <a href="#" title="">{{$deal->date}}</a>
                                                                </div>
                                                            </div>
                                                            @if ($deal->flighttype == 1)
                                                            International
                                                            @else
                                                            Domestic
                                                            @endif
                                                            <div class="price-box">
                                                                <span class="price old-price">From : {{$deal->flightto}}</span>
                                                                <span class="price special-price"><i class="fa fa-inr"></i>{{$deal->price}}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                @if ($count == 2)
                                            </div>
                                            @endif
                                            <?php $count++;?>
                                            @endforeach
                                            <!-- HostSales Item -->
                                            
                                            
                                        </div>
                                    </div>
                                </div>
                                <div id="section5" class="tab-pane fade">
                                    <div class="sales-cn" style="margin-top: 2%;">
                                             <div class="row">  {{--Row Start--}}
                                            <?php $count = 0; ?>
                                            @foreach ($deals as $deal)
                                             @if ($deal->flighttype == 1)
                                            @if ($count == 2)
                                            <div class="row">
                                                @endif
                                                <div class="col-xs-6 col-md-4">
                                                    <div class="sales-item">
                                                        <figure class="home-sales-img">
                                                            <a href="#" title="">
                                                                <img src="{{URL::asset($deal->cityimage)}}" onmouseover="this.src='{{URL::asset($deal->flightimage)}}'" onmouseout="this.src='{{URL::asset($deal->cityimage)}}'" style="height: 293px;" border="0" alt="">
                                                            </a>
                                                            <figcaption>
                                                            Save<span><br>{{$deal->discount}}</span>%
                                                            </figcaption>
                                                        </figure>
                                                        <div class="home-sales-text">
                                                            <div class="home-sales-name-places">
                                                                <div class="home-sales-name">
                                                                    <a href="#" title="">{{$deal->flightfrom}}</a>
                                                                </div>
                                                                <div class="home-sales-places">
                                                                    <a href="#" title="">{{$deal->date}}</a>
                                                                </div>
                                                            </div>
                                                            @if ($deal->flighttype == 1)
                                                            International
                                                            @else
                                                            Domestic
                                                            @endif
                                                            <div class="price-box">
                                                                <span class="price old-price">From : {{$deal->flightto}}</span>
                                                                <span class="price special-price">{{$deal->price}}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                @if ($count == 2)
                                            </div>
                                            @endif
                                            <?php $count++;?>
                                            @endif
                                            @endforeach
                                            <!-- HostSales Item -->
                                            
                                            
                                        </div>
                                    </div>
                                </div>
                                
                                <div id="section6" class="tab-pane fade">
                                    <div class="sales-cn" style="margin-top: 2%;">
                                             <div class="row">  {{--Row Start--}}
                                            <?php $count = 0; ?>
                                            @foreach ($deals as $deal)
                                            @if ($deal->flighttype == 0)
                                            @if ($count == 2)
                                            <div class="row">
                                                @endif
                                                <div class="col-xs-6 col-md-4">
                                                    <div class="sales-item">
                                                        <figure class="home-sales-img">
                                                            <a href="#" title="">
                                                                <img src="{{URL::asset($deal->cityimage)}}" onmouseover="this.src='{{URL::asset($deal->flightimage)}}'" onmouseout="this.src='{{URL::asset($deal->cityimage)}}'" style="height: 293px;" border="0" alt="">
                                                            </a>
                                                            <figcaption>
                                                            Save<span><br>{{$deal->discount}}</span>%
                                                            </figcaption>
                                                        </figure>
                                                        <div class="home-sales-text">
                                                            <div class="home-sales-name-places">
                                                                <div class="home-sales-name">
                                                                    <a href="#" title="">{{$deal->flightfrom}}</a>
                                                                </div>
                                                                <div class="home-sales-places">
                                                                    <a href="#" title="">{{$deal->date}}</a>
                                                                </div>
                                                            </div>
                                                            @if ($deal->flighttype == 1)
                                                            International
                                                            @else
                                                            Domestic
                                                            @endif
                                                            <div class="price-box">
                                                                <span class="price old-price">From : {{$deal->flightto}}</span>
                                                                <span class="price special-price">{{$deal->price}}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                @if ($count == 2)
                                            </div>
                                            @endif
                                            <?php $count++;?>
                                             @endif
                                            @endforeach
                                            <!-- HostSales Item -->
                                            
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>
@stop