<div class="container">
                <div class="row">
                    <!-- Logo -->
                    <div class="col-md-4">
                        <div class="">
                            <a href="#" title=""><img src="images/logo1.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-5">
                      <div class="follow-us">
                          <h4>Follow us</h4>
                          <div class="follow-group">
                              <a href="#" title=""><i class="fa fa-facebook"></i></a>
                              <a href="#" title=""><i class="fa fa-twitter"></i></a>
                              <a href="#" title=""><i class="fa fa-linkedin"></i></a>
                              <a href="#" title=""><i class="fa fa-instagram"></i></a>
                              <a href="#" title=""><i class="fa fa-google-plus"></i></a>

                          </div>
                      </div>
                    </div>
                    <!-- End Logo -->
                    <!-- End Navigation Footer -->
                    <!-- Footer Currency, Language -->
                    <div class="col-sm-6 col-md-3">
                        <p class="copyright">
                            © 2015 JetIt. All rights reserved.
                        </p>
                        <!--CopyRight-->
                    </div>
                    <!-- End Footer Currency, Language -->
                </div>
            </div>