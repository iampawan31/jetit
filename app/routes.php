<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

//Default Pages
Route::get('/', 'IndexController@index');
Route::get('/get-in-touch', array('as' => 'gettouch','uses' => 'IndexController@getintouch'));
Route::get('/grow-your-business', array('as' => 'growbusiness','uses' => 'IndexController@growbusiness'));
Route::get('/on-demand-charter', array('as' => 'demand','uses' => 'IndexController@demand'));
Route::get('/about-us', array('as' => 'about','uses' => 'IndexController@about'));



//Deals Pages
Route::get('/deals', array('as' => 'jetdeals','uses' => 'DealController@jetdeals'));
Route::post('/deals', array('as' => 'jetdeals','uses' => 'DealController@sortdeals'));
Route::get('/bookdeal/{id}', array('as' => 'deal','uses' => 'DealController@deal'));

//Admin Pages
Route::get('/admin-dashboard', array('as' => 'admin','uses' => 'AdminController@index'));
Route::post('/insertdeal', 'AdminController@insertdeal');
Route::get('/login', 'AdminController@login');
Route::post('/login', 'AdminController@logincheck');
Route::get('/deallog', array('as' => 'history','uses' => 'AdminController@history'));
Route::get('/charter', array('as' => 'charter','uses' => 'AdminController@charter'));
Route::get('/multileg', array('as' => 'multileg','uses' => 'AdminController@multileg'));
Route::get('logout',array('as' => 'logout', 'uses' => function(){
	Auth::logout();
	return Redirect::to('/');
}));

//CRUD Controllers
Route::resource('crud', 'CrudController');

//Fetching Values
Route::get('/fetchairports','FormController@fetchairports');
Route::post('/insertreg','FormController@insertreg');
Route::post('/insertmultiway','FormController@insertmultiway');
Route::get('/hash',function(){
	return Hash::make('thakur9895');
});






