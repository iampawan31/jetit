<?php

class CrudController extends \BaseController {

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		// Named Route
		$data = array(
			'title' => 'Jetit -Admin');
		// get the nerd
        $deal = Deal::find($id);

        // show the edit form and pass the nerd
        return View::make('pages.admin.editdeal',$data)
            ->with('deal', $deal);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$flightfrom = Input::get('flightfrom');
		$flighto = Input::get('flightto');
		$departuredate = Input::get('departuredate');
		$departuretime = Input::get('departuretime');
		$numberofpassengers = Input::get('numberofpassengers');
		$price = Input::get('price');
		$expirydate = Input::get('expirydate');
		//$mainimage = Input::get('mainimage');
		//$hoverimage = Input::get('hoverimage');
		$aircrafttype = Input::get('aircrafttype');
		$aircraftname = Input::get('aircraftname');
		$flighttype = Input::get('flighttype');
		$discount = Input::get('discount');
		$dateofpost = date('Y-m-d H:i:s');

		$rules = [
		'flightfrom' => 'required',
		'flightto' => 'required',
		'departuredate' => 'required',
		'departuretime' => 'required',
		'numberofpassengers' => 'required',
		'price' => 'required',
		'expirydate' => 'required',
		'mainimage' => 'required',
		'hoverimage' => 'required',
		'aircrafttype' => 'required',
		'aircraftname' => 'required',
		'flighttype' => 'required',
		'discount' => 'required'
		];

		$date = date('Y-m-d', strtotime($departuredate));
		$date1 = date('Y-m-d', strtotime($dateofpost));
			// echo $flighfrom;
			// echo $flighto;
			// echo $departuredate;
			// echo $departuretime;
			//  echo $date;


		$validator = Validator::make(Input::all(), $rules);

		if($validator->passes()){
			if (Input::file('mainimage')->isValid()){
				      $destinationPath = 'uploads/images/deals/'; // upload path
				      $extension = Input::file('mainimage')->getClientOriginalExtension(); // getting image extension
				      $fileName = rand(1111111,9999999).'.'.$extension; // renameing image
				      Input::file('mainimage')->move($destinationPath, $fileName);
				  }
				  if(Input::file('hoverimage')->isValid()){
				      $extension1 = Input::file('hoverimage')->getClientOriginalExtension(); // getting image extension
				      $fileName1 = rand(1111111,9999999).'.'.$extension1; // renameing image
				      Input::file('hoverimage')->move($destinationPath, $fileName1);
				  }
				   $deal = Deal::find($id);
				  //$deal = new Deal;
				  $deal->date = $date;
				  $deal->flighttime =  $departuretime;
				  $deal->flightfrom = $flightfrom;
				  $deal->flightto = $flighto;
				  $deal->expires = $expirydate;
				  $deal->flightimage = $destinationPath.$fileName1;
				  $deal->cityimage = $destinationPath.$fileName;
				  $deal->price = $price;
				  $deal->capacity = $numberofpassengers;
				  $deal->aircrafttype = $aircrafttype;
				  $deal->aircraftname = $aircraftname;
				  $deal->flighttype = $flighttype;
				  $deal->discount = $discount;
				  $deal->dateofpost = $date1;
				  $saved = $deal->save();

				  if(!$saved){
				  	App::abort(500,'Error');
				  }
				  else
				  {
				  	Session::put('dealupdate', 'Deal has been updated!');
				  	return Redirect::to('deallog');
				  }

				}
				else
				{
					return Redirect::to('crud/' . $id . '/edit')->withErrors($validator,'deal')->withInput();
				}
			}
	


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		 $deal = Deal::find($id);
        $deal->delete();

        Session::put('dealupdate', 'Deal has been Deleted!');
        return Redirect::to('deallog');
	}


}
