<?php
class IndexController extends BaseController {
/**
* Show the profile for the given user.
*/
public function index()
{
	$deals = Deal::orderBy('dateofpost', 'DESC')->get();
$data = array(
        'title' => 'Jetit - Homepage');
        return View::make('index', $data)->with('deals',$deals);
}
public function getintouch()
{
    $data = array(
'title' => 'Jetit - Get In Touch');
return View::make('gettouch')->with($data);
}
public function growbusiness()
{
    $data = array(
'title' => 'Jetit - Get Business');
return View::make('getbusiness')->with($data);
}

public function demand()
{
    // Named Route
$data = array(
'title' => 'Jetit - On Demand Charter');
return View::make('demand')->with($data);
}
public function about()
{
    // Named Route
$data = array(
'title' => 'Jetit - About Us');
return View::make('about')->with($data);
}

}