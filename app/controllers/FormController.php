<?php

class FormController extends BaseController {

   public function fetchairports()
    {

      $term = Input::get('term');
      $data = DB::table("airports")
              ->where('airport_name','LIKE','%'.$term.'%')
              ->orwhere('airport_id','LIKE','%'.$term.'%')
              ->orwhere('airport_country','LIKE','%'.$term.'%')->take(50)->get();
                if($data){
      foreach ($data as $v) {
        $return_array[] = ['value' => $v->airport_name.",".$v->airport_country.'('.$v->airport_id.')'];

      }

      return Response::json($return_array);
    }

}
public function insertreg()
    {
        $flightfrom = Input::get('from1');
        $flighto = Input::get('to1');
        $departuredate = Input::get('departuredate');
        $departuretime = Input::get('departuretime');
        $returndate = Input::get('returndate');
        $returntime = Input::get('returntime');
        $numberofpassengers = Input::get('numberofpassengers');
        $jettype = Input::get('jettype');
        $fistname = Input::get('fistname');
        //$mainimage = Input::get('mainimage');
        //$hoverimage = Input::get('hoverimage');
        $lastname = Input::get('lastname');
        $contact = Input::get('contact');
        $mail = Input::get('mail');
        $comment = Input::get('comment');
        $dateofregister = date('Y-m-d H:i:s');

        $rules = [
        'from1' => 'required',
        'to1' => 'required',
        'departuredate' => 'required',
        'departuretime' => 'required',
        'numberofpassengers' => 'required',
        'jettype' => 'required',
        'fistname' => 'required',
        'lastname' => 'required',
        'contact' => 'required',
        'mail' => 'required',
        'comment' => 'required',
        
        ];

        $date = date('Y-m-d', strtotime($departuredate));
        $date1 = date('Y-m-d', strtotime($returndate));
        $date2 = date('Y-m-d', strtotime($dateofregister));
            // echo $flighfrom;
            // echo $flighto;
            // echo $departuredate;
            // echo $departuretime;
            //  echo $date;


        $validator = Validator::make(Input::all(), $rules);

        if($validator->passes()){
            
                  $register = new Register;
                  $register->flightfrom = $flightfrom;
                  $register->flightto =  $flighto;
                  $register->departuredate = $date;
                  $register->departuretime = $departuretime;
                  $register->returntime = $returntime;
                  $register->noofpassangers = $numberofpassengers;
                  $register->jettype = $jettype;
                  $register->firstname = $fistname;
                  $register->lastname = $lastname;
                  $register->contact = $contact;
                  $register->email = $mail;
                  $register->comments = $comment;
                  $register->dateofregister = $date2;
                  $saved = $register->save();

                  if(!$saved){
                    App::abort(500,'Error');
                  }
                  else
                  {
                    Mail::send('mailers', array('registers'=>$register), function($massage)
                    {
                        $massage->to(Input::get('email'),Input::get('fistname'))->subject('Welcome to hemant');
                    });
                     return Redirect::to('admin-dashboard')->withErrors($validator,'register')->withInput();
                  }

                }
                else
                {
                    return Redirect::to('admin-dashboard')->withErrors($validator,'register')->withInput();
                }
            }

//for multiway form

    public function insertmultiway()
    {
      
        
        echo $flightfrom = Input::get('from2');
        echo "<br>";
        echo $flighto = Input::get('to2');
        echo "<br>";
        echo $departuredate = Input::get('departuredate11');
        echo "<br>";
        echo $departuretime = Input::get('departuretime11');
        echo "<br>";
        echo $numberofpassengers = Input::get('numberofpassengers1');
        echo "<br>";
        echo $jettype = Input::get('jettype1');
        echo "<br>";
        echo $fistname = Input::get('fistname1');
        echo "<br>";
        //$mainimage = Input::get('mainimage');
        //$hoverimage = Input::get('hoverimage');
        echo $lastname = Input::get('lastname1');
        echo "<br>";
        echo $contact = Input::get('contact1');
        echo "<br>";
        echo $mail = Input::get('mail1');
        echo "<br>";
        echo $flightlegfrom1 = Input::get('flightlegfrom1');
        echo $flightlegfrom2 = Input::get('flightlegfrom2');
        echo $flightlegfrom3 = Input::get('flightlegfrom3');
        echo $flightlegfrom4 = Input::get('flightlegfrom4');
        echo $flightlegfrom5 = Input::get('flightlegfrom5');
        echo $flightlegfrom6 = Input::get('flightlegfrom6');
        echo $flightlegfrom7 = Input::get('flightlegfrom7');
        echo $flightlegfrom8 = Input::get('flightlegfrom8');
        echo $flightlegfrom9 = Input::get('flightlegfrom9');
        echo $flightlegfrom10 = Input::get('flightlegfrom10');
        echo "<br>";
        echo $flightlegto1 = Input::get('flightlegto1');
        echo $flightlegto2 = Input::get('flightlegto2');
        echo $flightlegto3 = Input::get('flightlegto3');
        echo $flightlegto4 = Input::get('flightlegto4');
        echo $flightlegto5 = Input::get('flightlegto5');
        echo $flightlegto6 = Input::get('flightlegto6');
        echo $flightlegto7 = Input::get('flightlegto7');
        echo $flightlegto8 = Input::get('flightlegto8');
        echo $flightlegto9 = Input::get('flightlegto9');
        echo $flightlegto10 = Input::get('flightlegto10');
        echo "<br>";
        echo $departuredate1 = Input::get('departuredate1');
        echo $departuredate2 = Input::get('departuredate2');
        echo $departuredate3 = Input::get('departuredate3');
        echo $departuredate4 = Input::get('departuredate4');
        echo $departuredate5 = Input::get('departuredate5');
        echo $departuredate6 = Input::get('departuredate6');
        echo $departuredate7 = Input::get('departuredate7');
        echo $departuredate8 = Input::get('departuredate8');
        echo $departuredate9 = Input::get('departuredate9');
        echo $departuredate10 = Input::get('departuredate10');
        echo "<br>";
        echo $departuretime1 = Input::get('departuretime1');
        echo $departuretime2 = Input::get('departuretime2');
        echo $departuretime3 = Input::get('departuretime3');
        echo $departuretime4 = Input::get('departuretime4');
        echo $departuretime5 = Input::get('departuretime5');
        echo $departuretime6 = Input::get('departuretime6');
        echo $departuretime7 = Input::get('departuretime7');
        echo $departuretime8 = Input::get('departuretime8');
        echo $departuretime9 = Input::get('departuretime9');
        echo $departuretime10 = Input::get('departuretime10');
        echo $add2 = Input::get('field_name1');
        echo "<br>";
        echo $add3 = Input::get('field_name2');
        echo "<br>";
        echo $add4 = Input::get('field_name3');
        echo "<br>";
        echo $comment = Input::get('comment1');
        echo "<br>";

        echo $dateofregister = date('Y-m-d H:i:s');

        $rules = [
        'from2' => 'required',
        'to2' => 'required',
        'departuredate1' => 'required',
        'departuretime1' => 'required',
        'numberofpassengers1' => 'required',
        'jettype1' => 'required',
        'fistname1' => 'required',
        'lastname1' => 'required',
        'contact1' => 'required',
        'mail1' => 'required',
        'comment1' => 'required',
        
        ];

        $date = date('Y-m-d', strtotime($departuredate));
        $date2 = date('Y-m-d', strtotime($dateofregister));
        $legdate1 = date('Y-m-d', strtotime($departuredate1));
        $legdate2 = date('Y-m-d', strtotime($departuredate2));
        $legdate3 = date('Y-m-d', strtotime($departuredate3));
        $legdate4 = date('Y-m-d', strtotime($departuredate4));
        $legdate5 = date('Y-m-d', strtotime($departuredate5));
        $legdate6 = date('Y-m-d', strtotime($departuredate6));
        $legdate7 = date('Y-m-d', strtotime($departuredate4));
        $legdate8 = date('Y-m-d', strtotime($departuredate8));
        $legdate9 = date('Y-m-d', strtotime($departuredate9));
        
            // echo $flighfrom;
            // echo $flighto;
            // echo $departuredate;
            // echo $departuretime;
            //  echo $date;


        $validator = Validator::make(Input::all(), $rules);

        if($validator->passes()){
            
                  $multiway = new Multiway;
                  $multiway->mway_id = $id;
                  $multiway->flightfrom = $flightfrom;
                  $multiway->flightto =  $flighto;
                  $multiway->departuredate = $date;
                  $multiway->departuretime = $departuretime;
                  $multiway->noofpassangers = $numberofpassengers;
                  $multiway->jettype = $jettype;
                  $multiway->firstname = $fistname;
                  $multiway->lastname = $lastname;
                  $multiway->contact = $contact;
                  $multiway->email = $mail;
                  $multiway->comments = $comment;
                  $multiway->dateofregister = $date2;
                  $saved = $multiway->save();
                  if(Input::has('flightlegfrom1'))
                  {
                    $leg = new Leg;
                    $leg->mway_id = $id;
                    $leg->flightfrom = $flightlegfrom1;
                    $leg->flightto = $flightlegto1;
                    $leg->departuredate = $legdate1;
                    $leg->departuretime = $departuretime1;
                    $saved = $leg->save();
                  }
                  if(Input::has('flightlegfrom2'))
                  {
                  $leg = new Leg;
                  $leg->mway_id = $id;
                  $leg->flightfrom = $flightlegfrom2;
                  $leg->flightto = $flightlegto2;
                  $leg->departuredate = $legdate2;
                  $leg->departuretime = $departuretime2;
                  $saved = $leg->save();
                  }
                  if(Input::has('flightlegfrom3'))
                  {
                  $leg = new Leg;
                  $leg->mway_id = $id;
                  $leg->flightfrom = $flightlegfrom3;
                  $leg->flightto = $flightlegto3;
                  $leg->departuredate = $legdate3;
                  $leg->departuretime = $departuretime3;
                  $saved = $leg->save();
                  }
                  if(Input::has('flightlegfrom4'))
                  {
                  $leg = new Leg;
                  $leg->mway_id = $id;
                  $leg->flightfrom = $flightlegfrom4;
                  $leg->flightto = $flightlegto4;
                  $leg->departuredate = $legdate4;
                  $leg->departuretime = $departuretime4;
                  $saved = $leg->save();
                  }
                  if(Input::has('flightlegfrom5'))
                  {
                  $leg = new Leg;
                  $leg->mway_id = $id;
                  $leg->flightfrom = $flightlegfrom5;
                  $leg->flightto = $flightlegto5;
                  $leg->departuredate = $legdate5;
                  $leg->departuretime = $departuretime5;
                  $saved = $leg->save();
                  }
                  if(Input::has('flightlegfrom6'))
                  {
                  $leg = new Leg;
                  $leg->mway_id = $id;
                  $leg->flightfrom = $flightlegfrom6;
                  $leg->flightto = $flightlegto6;
                  $leg->departuredate = $legdate6;
                  $leg->departuretime = $departuretime6;
                  $saved = $leg->save();
                  }
                  if(Input::has('flightlegfrom7'))
                  {
                  $leg = new Leg;
                  $leg->mway_id = $id;
                  $leg->flightfrom = $flightlegfrom7;
                  $leg->flightto = $flightlegto7;
                  $leg->departuredate = $legdate7;
                  $leg->departuretime = $departuretime7;
                  $saved = $leg->save();
                  }
                  if(Input::has('flightlegfrom8'))
                  {
                  $leg = new Leg;
                  $leg->mway_id = $id;
                  $leg->flightfrom = $flightlegfrom8;
                  $leg->flightto = $flightlegto8;
                  $leg->departuredate = $legdate8;
                  $leg->departuretime = $departuretime8;
                  $saved = $leg->save();
                  }
                  if(Input::has('flightlegfrom9'))
                  {
                  $leg = new Leg;
                  $leg->mway_id = $id;
                  $leg->flightfrom = $flightlegfrom9;
                  $leg->flightto = $flightlegto9;
                  $leg->departuredate = $legdate9;
                  $leg->departuretime = $departuretime9;
                  $saved = $leg->save();
                }
                  if(!$saved){
                    App::abort(500,'Error');
                  }
                  else
                  {
                    return Redirect::to('admin1')->withErrors($validator,'multileg')->withInput();
                  }

                }
                else
                {
                    return Redirect::to('admin')->withErrors($validator,'multileg')->withInput();
                }
            }

}
