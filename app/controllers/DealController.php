<?php
class DealController extends \BaseController {
	public function jetdeals()
	{
// Named Route
		$deals = Deal::all();
		$data = array(
			'title' => 'Jetit - Deals');
		return View::make('deals',$data)->with('deals',$deals);
	}
	public function sortdeals()
	{
		//$flag = 1;
		$data = array(
			'title' => 'Jetit - Deals');
		$dealflightfrom = Input::get('dealflightfrom');
		$dealflightto = Input::get('dealflightto');
		$dealdeparturedate = Input::get('dealdeparturedate');
		$dealflighttype = Input::get('dealflighttype');
		// //echo $dealflighttype;
		// if ($dealflighttype == "1") {
			// 	$flag = 0;
		// }
		//echo $flag;
		$date = date('Y-m-d', strtotime($dealdeparturedate));
		$result = Deal::where('from',$dealflightfrom)->where('to',$dealflightto)->where('date',$date)->where(
			'aircrafttype',$dealflighttype)->get();
		if($result->count()){
			return View::make('getdeals',$data)->with('result',$result);
		}
		else{
			$msg = "<p><b>Sorry, we couldn't find any Deals matching all your requirements.</b> We expanded your search to all departure cities</p> <p> Some empty legs may be flying in the same direction and on the same date you're looking for.<br> Email our operations team at <b>info@jetit.in</b> and they will be delighted to quote for your particular requirement.</p>";
			$result1 = Deal::where('from',$dealflightfrom)->where('to',$dealflightto)->where('date',$date)->get();
			if($result1->count()){
				return View::make('getdeals',$data)->with('result',$result1)->with('msg',$msg);
			}
			else{
				$result2 = Deal::where('from',$dealflightfrom)->where('to',$dealflightto)->get();
				if($result2->count()){
					return View::make('getdeals',$data)->with('result',$result2)->with('msg',$msg);
				}
				else
				{
					$result3 = Deal::all();
				//var_dump($result3);
					if($result3){
						return View::make('getdeals',$data)->with('result',$result3)->with('msg',$msg);
					}
					else{
						return View::make('getdeals',$data)->with('result',$result3)->with('msg',$msg);
					}
				}
			}
		}
	}
	public function deal($id)
	{
// Named Route
		$deal = Deal::find($id);
		$data = array(
			'title' => 'Jetit - Book A Deal');
		return View::make('bookdeal',$data)->with('deal',$deal);
	}
	
}