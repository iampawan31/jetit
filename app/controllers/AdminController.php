<?php
//Admin Class
class AdminController extends \BaseController {
	public function index()
	{
		if (Auth::check())
		{
			$data = array(
			'title' => 'Jetit -Admin Dashboard');
		return View::make('pages.admin.dashboard')->with($data);
		}
		return Redirect::to('login');
		
	}
	public function login()
	{
		// Named Route
		$data = array(
			'title' => 'Jetit -Admin Login');
		return View::make('pages.admin.login')->with($data);
	}

	public function logincheck()
	{
		echo $username = Input::get('username');
		echo $password = Input::get('password');
		if (Auth::attempt(array('username' => $username, 'password' => $password)))
		{
			return Redirect::to('admin-dashboard');
		}
		else
		{
			echo "fail";
		}
		
	}
	public function history()
		{
			$deals = Deal::paginate(10);
   
		    // Named Route
		$data = array(
		'title' => 'Jetit - deal log page');
		return View::make('pages.admin.deallog', compact('deals'), $data)->with('deals',$deals);
		}

	public function charter()
		{
			$registers = Register::paginate(10);
   
		    // Named Route
		$data = array(
		'title' => 'Jetit - Charter log page');
		return View::make('pages.admin.charterlog', compact('registers'), $data)->with('registers',$registers);
		}

	
	
	public function insertdeal()
	{
		$flightfrom = Input::get('flighfrom');
		$flighto = Input::get('flighto');
		$departuredate = Input::get('departuredate');
		$departuretime = Input::get('departuretime');
		$numberofpassengers = Input::get('numberofpassengers');
		$price = Input::get('price');
		$expirydate = Input::get('expirydate');
		//$mainimage = Input::get('mainimage');
		//$hoverimage = Input::get('hoverimage');
		$aircrafttype = Input::get('aircrafttype');
		$aircraftname = Input::get('aircraftname');
		$flighttype = Input::get('flighttype');
		$discount = Input::get('discount');
		$dateofpost = date('Y-m-d H:i:s');

		$rules = [
		'flighfrom' => 'required',
		'flighto' => 'required',
		'departuredate' => 'required',
		'departuretime' => 'required',
		'numberofpassengers' => 'required',
		'price' => 'required',
		'expirydate' => 'required',
		'mainimage' => 'required',
		'hoverimage' => 'required',
		'aircrafttype' => 'required',
		'aircraftname' => 'required',
		'flighttype' => 'required',
		'discount' => 'required'
		];

		$date = date('Y-m-d', strtotime($departuredate));
		$date1 = date('Y-m-d', strtotime($dateofpost));
			// echo $flighfrom;
			// echo $flighto;
			// echo $departuredate;
			// echo $departuretime;
			//  echo $date;


		$validator = Validator::make(Input::all(), $rules);

		if($validator->passes()){
			if (Input::file('mainimage')->isValid()){
				      $destinationPath = 'uploads/images/deals/'; // upload path
				      $extension = Input::file('mainimage')->getClientOriginalExtension(); // getting image extension
				      $fileName = rand(1111111,9999999).'.'.$extension; // renameing image
				      Input::file('mainimage')->move($destinationPath, $fileName);
				  }
				  if(Input::file('hoverimage')->isValid()){
				      $extension1 = Input::file('hoverimage')->getClientOriginalExtension(); // getting image extension
				      $fileName1 = rand(1111111,9999999).'.'.$extension1; // renameing image
				      Input::file('hoverimage')->move($destinationPath, $fileName1);
				  }
				  $deal = new Deal;
				  $deal->date = $date;
				  $deal->flighttime =  $departuretime;
				  $deal->flightfrom = $flightfrom;
				  $deal->flightto = $flighto;
				  $deal->expires = $expirydate;
				  $deal->flightimage = $destinationPath.$fileName1;
				  $deal->cityimage = $destinationPath.$fileName;
				  $deal->price = $price;
				  $deal->capacity = $numberofpassengers;
				  $deal->aircrafttype = $aircrafttype;
				  $deal->aircraftname = $aircraftname;
				  $deal->flighttype = $flighttype;
				  $deal->discount = $discount;
				  $deal->dateofpost = $date1;
				  $saved = $deal->save();

				  if(!$saved){
				  	App::abort(500,'Error');
				  }
				  else
				  {
				  	Session::put('dealupdate', 'Deal has been added');
				  	return Redirect::to('admin-dashboard');
				  }

				}
				else
				{
					return Redirect::to('admin-dashboard')->withErrors($validator,'deal')->withInput();
				}
			}
		}